const Rental = require('../models/rental')

async function getRental(rentalId) {
    const rental = await Rental.findById(rentalId)
        .select(['-__v'])
        .populate('customer')
        .populate('city')
        .populate('product')
        .lean()

    return rental
}

module.exports = {
    getRental,
}