const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
	completeSet: {
		type: [String],
		required: true,
	},
	pricing: {
		type: {
			deposit: {
				type: Number,
				required: true,
			},
			fees: {
				type: [Number],
				required: true,
			},
		},
		required: true,
	},
	image: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'File',
		required: true,
	},
	city: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'City',
		required: true,
	},
	category: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Category',
		required: true,
	},
})

const Product = mongoose.model('Product', productSchema)

module.exports = Product
