const User = require('../../models/user')
const { google } = require('googleapis')
const { googleClient } = require('../../../config')

const SCOPES = [
	'https://www.googleapis.com/auth/calendar', 
	'https://www.googleapis.com/auth/contacts',
]

function getOAuth2Client() {
	const { client_secret, client_id, redirect_uris } = googleClient
	const oAuth2Client = new google.auth.OAuth2(
		client_id,
		client_secret,
		redirect_uris[0],
	)
	return oAuth2Client
}

async function authorize(userId) {
	const user = await User.findById(userId)
	const token = JSON.parse(user.googleAccessToken)
	console.log(token)
	if (!token) { return }
	
	oAuth2Client = getOAuth2Client()
    oAuth2Client.setCredentials(token)
    return oAuth2Client
}

function generateAuthenticationUrl() {
	oAuth2Client = getOAuth2Client()

	const url = oAuth2Client.generateAuthUrl({
		access_type: 'offline',
		scope: SCOPES,
	})

	return url
}

async function validateAuthenticationCode(code, userId) {
	oAuth2Client = getOAuth2Client()
	
	const token = await oAuth2Client.getToken(code)
	await setGoogleAuthToken(token.tokens, userId)
}

async function setGoogleAuthToken(token, userId) {
	const user = await User.findById(userId)
	user.googleAccessToken = JSON.stringify(token)
	console.log(token)
	await user.save()
}

module.exports = {
	generateAuthenticationUrl,
    validateAuthenticationCode,
    authorize,
}