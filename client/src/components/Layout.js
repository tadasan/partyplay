import React, { useEffect, useState } from 'react'
import { Route, Link } from 'react-router-dom'
import Logo from '../img/logo.png'
import axios from 'axios'
import { apiUrl } from '../config'
import Hero from './Hero'
import CitySelect from './CityList'
import CategoryList from './CategoryList'
import '../style/style.css'

function App() {
	const [cities, setCities] = useState([])

	useEffect(() => {
		getCities()
	}, [])

	async function getCities() {
		try {
			const response = await axios.get(apiUrl + '/api/client/cities')
			setCities(response.data)
		} catch (error) {}
	}

	return (
		<>
			<nav className='navbar navbar-expand-md sticky-top shadow' id='main-nav'>
				<div className='container'>
					<Link className='navbar-brand' to='/'>
						<span>
							<img className='logo' src={Logo} alt='' />
						</span> Party Play
					</Link>
					<button className='navbar-toggler' data-toggle='collapse' data-target='#navbarCollapse'>
						<span className='fas fa-bars' />
					</button>
				</div>
			</nav>
			<Hero />
			<div>
				<Route exact path='/' component={() => <CitySelect cities={cities} />} />
				<Route exact path='/:cityName' component={() => <CategoryList />} />
				<Route exact path='/:cityName/:categoryName' component={() => <div>Empty</div>} />
			</div>
		</>
	)
}

export default App
