const User = require('../models/user')
const { validateJSON } = require('../util')
const cookie = require('cookie')
const { jwtKey, sessionName } = require('../../config')
const jwt = require('jsonwebtoken')
const wrap = require('express-async-wrap')
const bcrypt = require('bcrypt')
const router = require('express').Router()
const { authentication } = require('../middleware/auth')
const { generateAuthenticationUrl, validateAuthenticationCode } = require('../services/googleServices/authService')
const { initializeContactGroup } = require('../services/googleServices/contactsService')

const userSchema = {
	type: 'object',
	properties: {
		email: {
			type: 'string',
			format: 'email',
		},
		password: {
			type: 'string',
		},
	},
	required: ['email', 'password'],
}

router.post('/', wrap(async (req, res) => {
	validateJSON(req.body, userSchema)

	const user = await User.findOne({ email: req.body.email })
	if (!user) { throw new Error('Unauthorized') }

	const validPassword = await bcrypt.compare(req.body.password, user.password)
	if (!validPassword) { throw new Error('Unauthorized') }

	const token = jwt.sign({ _id: user._id, email: user.email }, jwtKey, { expiresIn: '2h' })
	res.setHeader('Set-Cookie', cookie.serialize(sessionName, token, { maxAge: 2*60*60, path: '/' }))
	res.sendStatus(201)
}))

router.get('/', authentication, wrap(async (req, res) => {
	res.json(req.user)
}))

router.get('/google/auth-url', authentication, wrap(async (req,res) => {
	const url = generateAuthenticationUrl()
	res.send(url)
}))

router.post('/google/validate-code', authentication, wrap(async (req,res) => {
	await validateAuthenticationCode(req.body.code, req.user._id)
	await initializeContactGroup(req.user._id)
	res.sendStatus(204)
}))

router.delete('/', wrap(async (req, res) => {
	res.setHeader('Set-Cookie', cookie.serialize(sessionName, '', { maxAge: 2 * 60 * 60, path: '/' }))
	res.sendStatus(204)
}))

module.exports = router
