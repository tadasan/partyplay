const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: true,
		unique: true,
	},
	userGroup: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'UserGroup',
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
	name: String,
	googleAccessToken: String,
	googleContactGroupResourceName: String,
})

userSchema.methods.setPassword = async function (password) {
	const salt = await bcrypt.genSalt(10)
  	this.password = await bcrypt.hash(password, salt)
}

const User = mongoose.model('User', userSchema)

module.exports = User
