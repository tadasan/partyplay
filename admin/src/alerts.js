import React, { useContext, useState, createContext, useCallback } from 'react'
import PropTypes from 'prop-types'

const AlertsContext = createContext()

export function ProvideAlerts({ children }) {
	const alerts = useProvideAlerts()
	return <AlertsContext.Provider value={alerts}>{children}</AlertsContext.Provider>
}

ProvideAlerts.propTypes = {
	children: PropTypes.any,
}

export const useAlerts = () => useContext(AlertsContext)

export function useProvideAlerts() {
	const [alert, setAlert] = useState({ text: '', variant: '', show: false })

	function showAlert(text, variant) {
		setAlert({ text, variant, show: true })
		setTimeout(() => {
			setAlert(alert => { return { ...alert, show: false }})
		}, 3000)
	}

	return { alert, showAlert }
}
