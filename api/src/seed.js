const User = require('./models/user')
const UserGroup = require('./models/userGroup')
const { adminEmail } = require('../config')

async function seedAdmin() {

	let adminGroup = await UserGroup.findOne({ name: 'Admin' })

	const adminGroupParams = {
		name: 'Admin',
		rights: UserGroup.rights,
	}

	if (!adminGroup) {
		adminGroup = await new UserGroup(adminGroupParams).save()
	} else {
		await adminGroup.updateOne(adminGroupParams)
	}

	let admin = await User.findOne({ email: adminEmail })
	const adminParams = {
		email: adminEmail,
		userGroup: adminGroup._id,
	}

	if (!admin) {
		admin = new User(adminParams)
	} else {
		await admin.updateOne(adminParams)
	}
	await admin.setPassword('admin')
	admin.save()
}

module.exports = seedAdmin
