const request = require('supertest')
const UserGroup = require('../../src/models/userGroup')
const City = require('../../src/models/city')
const mongoose = require('mongoose')
const { sessionName } = require('../../config')
const { removeAllCollections, getJwtToken, server } = require('../testUtil')

describe('/api/cities', () => {

	beforeAll(async () => {
		const url = 'mongodb://localhost:27017/citiesDbb'
  		await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
		await removeAllCollections()
	})

	describe('GET /', () => {

		beforeEach(async () => {
			await new City({ name: 'City1', address: 'Some address' }).save()
			await new City({ name: 'City2', address: 'Some address' }).save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should return all cities', async () => {
			const res = await request(server)
				.get('/api/cities/')
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesRead'])}`)

			expect(res.status).toBe(200)
			expect(res.body.some(p => p.name === 'City1')).toBeTruthy()
			expect(res.body.some(p => p.name === 'City2')).toBeTruthy()
		})

		it('should throw 401 if jwt token is not provided', async () => {
			const res = await request(server)
				.get('/api/cities/')

			expect(res.status).toBe(401)
		})

		it('should throw 403 if CitiesRead right is not provided', async () => {
			const res = await request(server)
				.get('/api/cities/')
				.set('Cookie', `${sessionName}=${await getJwtToken([])}`)

			expect(res.status).toBe(403)
		})
	})

	describe('GET /:id', () => {
		let city

		beforeEach(async () => {
			city = await new City({ name: 'City1', address: 'Some address' }).save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should return City if valid ID is passed', async () => {
			const res = await request(server)
				.get(`/api/cities/${city._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesRead'])}`)

			expect(res.status).toBe(200)
			expect(res.body).toHaveProperty('name', city.name)
		})

		it('should throw 400 if City ID is invalid', async () => {
			const res = await request(server)
				.get('/api/cities/123')
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesRead'])}`)

			expect(res.status).toBe(400)
		})

		it('should throw 401 if jwt token is not provided', async () => {
			const res = await request(server)
				.get(`/api/cities/${city._id}`)

			expect(res.status).toBe(401)
		})

		it('should throw 404 if no Ctiy was found with provided valid ID', async () => {
			const randomId = mongoose.Types.ObjectId()
			const res = await request(server)
				.get(`/api/cities/${randomId}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesRead'])}`)

			expect(res.status).toBe(404)
		})
	})

	describe('POST /', () => {
		const cityDto = { name: 'City1', address: 'Some address' }

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should save the City if DTO is valid', async () => {
			const res = await request(server)
				.post('/api/cities')
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesWrite'])}`)
				.send(cityDto)

			const createdId = res.header.location.split('/').pop()
			const created = await City.findById(createdId)

			expect(res.status).toBe(201)
			expect(created).not.toBeNull()
		})

		it('should return 400 if name is missing', async () => {
			const res = await request(server)
				.post('/api/cities')
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesWrite'])}`)
				.send({ ...cityDto, name: undefined })

			expect(res.status).toBe(400)
		})

		it('should return 400 if City with same name already exists', async () => {
			const jwt = await getJwtToken(['CitiesWrite'])
			await request(server)
				.post('/api/cities')
				.set('Cookie', `${sessionName}=${jwt}`)
				.send(cityDto)

			const res =	await request(server)
				.post('/api/cities')
				.set('Cookie', `${sessionName}=${jwt}`)
				.send(cityDto)

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('CityNameAlreadyInUse')
		})


		it('should throw 401 if jwt token is not provided', async () => {
			const res = await request(server)
				.post('/api/cities')
				.send(cityDto)

			expect(res.status).toBe(401)
		})

		it('should throw 403 if CitiesWrite right is not provided', async () => {
			const res = await request(server)
				.post('/api/cities')
				.set('Cookie', `${sessionName}=${await getJwtToken([])}`)
				.send(cityDto)

			expect(res.status).toBe(403)
		})
	})

	describe('PUT /:id', () => {
		const cityDto = { name: 'City1', address: 'Some address' }
		let city

		beforeEach(async () => {
			city = await new City({ name: 'City1', address: 'Some address' }).save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should update City if DTO is valid', async () => {
			const res = await request(server)
				.put(`/api/cities/${city._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesWrite'])}`)
				.send(cityDto)

			expect(res.status).toBe(204)

			const updated = await City.findById(city._id)
			expect(updated.name).toBe(cityDto.name)
		})

		it('should return 400 if name is missing', async () => {
			const res = await request(server)
				.put(`/api/cities/${city._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesWrite'])}`)
				.send({ ...cityDto, name: undefined })

			expect(res.status).toBe(400)
		})

		it('should return 404 if city with the given id was not found', async () => {
			const randomId = mongoose.Types.ObjectId()

			const res = await request(server)
				.put(`/api/cities/${randomId}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesWrite'])}`)
				.send(cityDto)

			expect(res.status).toBe(404)
		})
	})

	describe('DELETE /:id', () => {
		let city

		beforeEach(async () => {
			city = await new City({ name: 'City1', address: 'Some address' }).save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should delete the City if ID is valid', async () => {
			const res = await request(server)
				.delete(`/api/cities/${city._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesDelete'])}`)

			expect(res.status).toBe(200)
			const deleted = await City.findById(city._id)
			expect(deleted).toBeNull()
		})

		it('should return 404 if no City with the given id was found', async () => {
			id = mongoose.Types.ObjectId()

			const res = await request(server)
				.delete(`/api/cities/${id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['CitiesDelete'])}`)

			expect(res.status).toBe(404)
		})

		it('should throw 403 if CitiesDelete right is not provided', async () => {
			const res = await request(server)
				.delete(`/api/cities/${city._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken([])}`)

			expect(res.status).toBe(403)
		})
	})
})
