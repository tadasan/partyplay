const dev = process.env.NODE_ENV !== 'production'

const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
module.exports = {
	entry: './src/index.js',
	mode: dev ? 'development' : 'production',
	output: {
		path: path.join(__dirname, '/dist'),
		filename: 'bundle.js',
		publicPath: './',
	},
	devServer: {
		historyApiFallback: true,
		port: 5000,
		proxy: {
			'/api': 'http://localhost:4000',
		},
	},
	plugins: [
		new HtmlWebpackPlugin({
			hash: true,
			template: './src/index.html',
		}),
		new MiniCssExtractPlugin({
			filename: 'bundle.css?v=[chunkhash]',
		}),
	],
	resolve: {
		extensions: ['*', '.js', '.jsx'],
		modules: [path.resolve('./src'), 'node_modules'],
	},
	module: {
		rules: [
			{
				test: /\.(jpg|png)$/,
				use: {
					loader: 'url-loader',
					options: {
						outputPath: 'img',
					},
				},
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				options: {
					presets: ['@babel/preset-env', '@babel/preset-react'],
					plugins: ['@babel/plugin-transform-runtime', 'inline-react-svg'],
				},
			},
			{
				test: /\.svg$/,
				use: [
					{
						loader: 'babel-loader',
					},
					{
						loader: 'react-svg-loader',
						options: {
							jsx: true,
						},
					},
				],
			},
			{
				test: /\.scss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					'css-loader',
					'sass-loader',
				],
			},
			{
				test: /\.css$/i,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							hmr: dev,
						},
					},
					'css-loader',
				],
			},
		],
	},
}
