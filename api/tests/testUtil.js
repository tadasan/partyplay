const UserGroup = require('../src/models/userGroup')
const User = require('../src/models/user')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const { jwtKey } = require('../config')

const server = require('../src/app')

async function removeAllCollections() {
	if (process.env='test') {
		const collections = Object.keys(mongoose.connection.collections)
		for (const collectionName of collections) {
		  const collection = mongoose.connection.collections[collectionName]
		  await collection.deleteMany()
		}
	}
}

async function getJwtToken(rights) {
	const jwtGroup = new UserGroup({ name: 'jwt', rights: rights })
	await jwtGroup.save()
	const jwtUser = new User({ email: 'jwt@test.tt', userGroup: jwtGroup._id })
	await jwtUser.setPassword('testtest')
	await jwtUser.save()

	return await jwt.sign({ _id: jwtUser._id, email: jwtUser.email }, jwtKey, { expiresIn: '2h' })
}

module.exports = {
	removeAllCollections,
	getJwtToken,
	server,
}
