import React, { useRef, useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import {
	Card,
	ButtonToolbar,
	Table,
	Button,
	ButtonGroup,
} from 'react-bootstrap'
import PlusIcon from 'mdi-react/PlusIcon'
import DeleteIcon from 'mdi-react/DeleteIcon'
import AccountEditIcon from 'mdi-react/AccountEditIcon'
import http from '../../http'
import { useAlerts } from '../../alerts'
import moment from 'moment'

function RentalList() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const [rentals, setRentals] = useState([])
	const [reloadTable, setReloadTable] = useState(false)

	useEffect(() => {
		getRentals()
	}, [reloadTable])

	async function getRentals() {
		try {
			const response = await http.get('/api/rentals')
			setRentals(response.data)
		} catch (error) {}
	}

	async function deleteRental(id) {
		try {
			await http.delete('/api/rentals/' + id)
			showAlert('Nuoma sėkmingai ištrinta', 'success')
			await setReloadTable(!reloadTable)
		} catch (error) {
			showAlert('Nepavyko ištrinti nuomos', 'danger')
		}
    }
    
    function humanizeStatus(status) {
        switch (status) {
            case 'reserved':
                return 'rezervuota'
            case 'ended': 
                return 'užbaigta'
            case 'active':
                return 'aktyvi'
            case 'canceled':
                return 'atšaukta'
            default: return ''
        }
    }

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5>Nuomos</h5>
						<ButtonToolbar>
							<Link to="/rentals/new">
								<Button variant="secondary" size="sm">
									<PlusIcon/> Kurti naują
								</Button>
							</Link>
						</ButtonToolbar>
					</Card.Title>
					<Table striped hover responsive>
						<thead>
							<tr>
								<th>Daiktas</th>
                                <th>Miestas</th>
								<th>Nuomininkas</th>
								<th>Pradžios data</th>
								<th>Pabaigos data</th>
                                <th>Kaina</th>
                                <th>Būsena</th>
								<th>Veiksmai</th>
							</tr>
						</thead>
						<tbody>
							{rentals.map((item) => (
								<tr key={item._id}>
									<td>{item.product.name}</td>
									<td>{item.city.name}</td>
									<td>{item.customer.phone}</td>
									<td>{moment(item.startDate).format('YYYY-MM-DD HH:mm')}</td>
                                    <td>{moment(item.endDate).format('YYYY-MM-DD HH:mm')}</td>
                                    <td>{item.fee} &euro;</td>
                                    <td>{humanizeStatus(item.status)}</td>
									<td>
										<ButtonGroup>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													deleteRental(item._id)
												}}
											>
												<DeleteIcon />
											</a>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													history.push(`/rentals/edit/${item._id}`)
												}}
											>
												<AccountEditIcon />
											</a>
										</ButtonGroup>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		</>
	)
}

export default RentalList
