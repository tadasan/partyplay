const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
	image: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'File',
	},
	products: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product', default: [] }],
})

const Category = mongoose.model('Category', categorySchema)

module.exports = Category
