# Daiktų rezervacijos bei valdymo informacinė sistema

Testavimo aplinkai paleisti atliekamos komandos:
– npm install - pagrindinėje direktorijoje;
– npm install ir npm dev - /api kataloge;
– npm install ir npm start - /admin kataloge;
– npm install ir npm start - /client kataloge.

Admin prisijungimo duomenys:
- email: admin@partyplay.lt
- password: admin

Numatytieji prievadai:

– 4000 – api;
– 3000 – client;
– 5000 – admin.


Testų vykdymas:
– statinei kodo analizei – npm lint - pagrindinėje direktorijoje;
– vienetų testams – npm test - /api kataloge.
!!! prieš paleidžiant npm test komandą būtinai išjunkite api serverį, priešingu atveju, gali būti ištrinti duomenų bazės duomenys
