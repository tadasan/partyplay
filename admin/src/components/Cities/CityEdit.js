import React, { useState, useEffect, useMemo } from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import { useParams, useHistory } from 'react-router-dom'
import http from '../../http'
import { useForm, ErrorMessage } from 'react-hook-form'
import Select from 'react-select'
import { useAlerts } from '../../alerts'

function CityEdit() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const { id: cityId } = useParams()
	const isNew = useMemo(() => cityId === 'new', [cityId])
	const { register, handleSubmit, errors, reset } = useForm({})

	useEffect(() => {
		if (cityId !== 'new') {
			getCity(cityId)
		}
	}, [cityId])

	async function getCity(id) {
		try {
			const response = await http.get(`/api/cities/${id}`)
			reset({
				name: response.data.name,
				address: response.data.address,
			})
		} catch (error) {}
	}

	async function createCity(dto) {
		try {
			const response = await http.post('/api/cities/', dto)
			showAlert('Miestas sėkmingai sukurtas', 'success')
			const newCityId = response.headers.location.split('/').pop()
			history.push(`/cities/${newCityId}`)
		} catch (error) {
			if (error.response.data.data === 'CityNameAlreadyInUse') {
				showAlert('Miesto vardas jau užimtas', 'danger')
			}
		}
	}

	async function editCity(dto) {
		try {
			await http.put(`/api/cities/${cityId}`, dto)
			showAlert('Miestas sėkmingai atnaujintas', 'success')
		} catch (error) {
			if (error.response.data.data === 'CityNameAlreadyInUse') {
				showAlert('Miesto vardas jau užimtas', 'danger')
			}
		}
	}

	const onSubmit = formData => {
		const dto = {
			name: formData.name,
			address: formData.address,
		}
		isNew ? createCity(dto) : editCity(dto)
	}

	return (
		<>
			<Card>
    	<Card.Body>
    		<Card.Title>
    			<h5>{isNew ? 'Naujas miestas' : 'Miesto redagavimas'}</h5>
    		</Card.Title>
    		<Card.Body>
    			<Form onSubmit={handleSubmit(onSubmit)}>
    				<Form.Group>
    					<Form.Label>Miesto pavadinimas</Form.Label>
    					<Form.Control
    						type='text'
    						name='name'
    						ref={register({ required: 'Šis laukelis privalomas' })}
    					/>
    					<ErrorMessage errors={errors} name='name' as='p' className='error-m' />
    				</Form.Group>
    				<Form.Group>
    					<Form.Label>Adresas</Form.Label>
    					<Form.Control
    						type='text'
    						name='address'
    						ref={register({ required: 'Šis laukelis privalomas' })}
    					/>
    					<ErrorMessage errors={errors} name='address' as='p' className='error-m' />
    				</Form.Group>
    				<Button variant='secondary' type='submit'>Patvirtinti</Button>
    			</Form>
    		</Card.Body>
    	</Card.Body>
			</Card>
		</>
	)
}

export default CityEdit
