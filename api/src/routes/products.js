const Product = require('../models/product')
const Category = require('../models/category')
const City = require('../models/city')
const { authentication, authorization } = require('../middleware/auth')
const { validateJSON, validateObjectID, getPaging } = require('../util')
const { ValidationError } = require('../errors')
const wrap = require('express-async-wrap')
const router = require('express').Router()

const productSchema = {
	type: 'object',
	properties: {
		name: {
			type: 'string',
		},
		description: {
			type: 'string',
		},
		completeSet: {
			type: 'array',
		},
		pricing: {
			type: 'object',
			properties: {
				deposit: {
					type: 'number',
				},
				fees: {
					type: 'array',
				},
			},
		},
		image: {
			type: 'string',
			pattern: '^[0-9a-fA-F]{24}$',
		},
		city: {
			type: 'string',
			pattern: '^[0-9a-fA-F]{24}$',
		},
		category: {
			type: 'string',
			pattern: '^[0-9a-fA-F]{24}$',
		},

	},
	required: ['name', 'description', 'completeSet', 'pricing', 'city', 'category', 'image'],
}

function setProductFields(productEntity, dto) {
	productEntity.name = dto.name
	productEntity.description = dto.description
	productEntity.completeSet = dto.completeSet
	productEntity.pricing = dto.pricing
	productEntity.image = dto.image
	productEntity.city = dto.city
	productEntity.category = dto.category
}

router.get('/', wrap(async (req, res) => {
	if (req && res && req.headers['return-total-count'] === 'true') {
		res.set('Total-Count', await Product.countDocuments({}))
	}

	const q = req.query.city ? { city: req.query.city} : {}
	const products = await Product.find(q)
		.select(['-__v'])
		.populate('city',)
		.populate('category')
		.lean()

	res.send(products)
}))

router.get('/:id', wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const product = await Product.findById(req.params.id)
		.select(['-__v'])
		.populate('city')
		.populate('category')
		.lean()

	if (!product) {
		throw new Error('NotFound')
	}

	res.send(product)
}))

router.post('/', authorization('ProductsWrite'), wrap(async (req, res) => {
	validateJSON(req.body, productSchema)

	const product = new Product()
	setProductFields(product, req.body)
	await product.save(async () => {
		const category = await Category.findById(req.body.category)
		if (!category) {
			throw new ValidationError('ProductCategoryNotFound')
		}
		category.products.push(product)
		await category.save()
	})

	res.set('Location', `${req.baseUrl}/${product._id}`)
	res.sendStatus(201)
}))

router.put('/:id', authorization('ProductsWrite'), wrap(async (req, res) => {
	validateObjectID(req.params.id)
	validateJSON(req.body, productSchema)

	const product = await Product.findById(req.params.id)
	if (!product) {
		throw new Error('NotFound')
	}

	setProductFields(product, req.body)
	await product.save()
	res.sendStatus(204)
}))

router.delete('/:id', authorization('ProductsDelete'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const product = await Product.findById(req.params.id)

	if (!product) {
		throw new Error('NotFound')
	}

	await product.delete(async () => {
		const category = await Category.findOne({ products: req.params.id })
		if (!category) {
			throw new ValidationError('ProductCategoryNotFound')
		}
		category.products = category.products.filter(product => product !== product._id)
		await category.save()
	})
	res.sendStatus(200)
}))

module.exports = router
