module.exports = {
	port: process.env.PORT || 4000,
	db: process.env.DB_NAME || 'mongodb+srv://tadusie:admin@cluster0-gxsq7.gcp.mongodb.net/partyplay?retryWrites=true&w=majority',
	jwtKey: process.env.JWT_PRIVATE_KEY || 'random long code',
	sessionName: process.env.SESSION_NAME || 'partyplay',
	adminEmail: process.env.ADMIN_EMAIL || 'admin@partyplay.lt',
	googleContactGroupName: 'Party Play',
	googleClient: {
		client_id: process.env.GOOGLE_CLIENT_ID || '1025379112214-9uiq0nqitfadf4e9hd5b5e6p6akgvvvg.apps.googleusercontent.com',
		project_id: process.env.GOOGLE_PROJECT_ID || 'quickstart-1588884859111',
		auth_uri: 'https://accounts.google.com/o/oauth2/auth',
		token_uri: 'https://oauth2.googleapis.com/token',
		auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
		client_secret: process.env.GOOGLE_CLIENT_SECRET || 'Fr7TpEEXMxnmsCX2qpmX_Rum',
		redirect_uris: ['https://partyplay-admin.netlify.app'],
		javascript_origins: ['https://api-partyplay.herokuapp.com'],
	},
}

//dbname: mongodb+srv://tadusie:admin@cluster0-gxsq7.gcp.mongodb.net/partyplay?retryWrites=true&w=majority
//mongodb://localhost:27017/partyplay