const router = require('express').Router()
const File = require('../models/file')
const wrap = require('express-async-wrap')
const fs = require('fs')
const { validateObjectID } = require('../util')
const { authentication, authorization } = require('../middleware/auth')
const multer = require('multer')

router.get('/:id', wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const file = await File.findOne({ _id: req.params.id }).select(['-__v'])

	if (!file) {
		throw new Error('NotFound')
	}

	res.contentType(file.contentType)
	res.send(file.buffer)
}))

let upload = multer({ dest: 'uploads/' })

router.use(authentication)

router.post('/', authorization('FilesWrite'), upload.single('file'), wrap(async (req, res) => {
	var buffer = fs.readFileSync(req.file.path)

	const file = new File()
	file.filename = req.file.filename
	file.buffer = buffer
	file.contentType = req.file.mimetype
	await file.save()

	res.json([{ filename: file.filename, _id: file._id }])
}))



module.exports = router
