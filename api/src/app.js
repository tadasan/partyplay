const { db } = require('../config')
const mongoose = require('mongoose')
const express = require('express')
const app = express()
const cors = require('cors')
const seed = require('./seed')
const logger = require('./logger')

mongoose
	.connect(db, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useCreateIndex: true,
		useFindAndModify: false,
	})
	.then(() => console.info('connected to database ' + db))
	.catch(err => console.error('error:', err))

seed()
app.use(cors({ 
	credentials: true,
	origin: [
		'https://partyplay-client.netlify.app',
		'https://partyplay-admin.netlify.app',
		'https://partyplay-admin.now.sh',
		'http://localhost:3000',
		'http://localhost:4000',
		'http://localhost:5000'
	],
}))

app.use(express.json())
app.use('/api', require('./api'))
app.use((err, req, res, next) => {
	if (err.message === 'ValidationError') {
		res.status(400).json({ data: err.data, message: err.message })
		logger.log({ level: 'warn', message: err.message + ' ' +err.data})
	} else if (err.message === 'NotFound') {
		res.sendStatus(404)
	} else if (err.message === 'Unauthorized') {
		logger.log({ level: 'warn', message: err.message})
		res.sendStatus(401)
	} else {
		logger.log({ level: 'error', message: err.message})
		res.sendStatus(500)
	}
})

module.exports = app
