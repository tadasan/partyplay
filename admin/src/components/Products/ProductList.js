import React, { useRef, useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import {
	Card,
	ButtonToolbar,
	Table,
	Button,
	ButtonGroup,
} from 'react-bootstrap'
import PlusIcon from 'mdi-react/PlusIcon'
import DeleteIcon from 'mdi-react/DeleteIcon'
import AccountEditIcon from 'mdi-react/AccountEditIcon'
import http from '../../http'
import { useAlerts } from '../../alerts'

function ProductList() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const [products, setProducts] = useState([])
	const [reloadTable, setReloadTable] = useState(false)

	useEffect(() => {
		getProducts()
	}, [reloadTable])

	async function getProducts() {
		try {
			const response = await http.get('/api/products')
			setProducts(response.data)
		} catch (error) {}
	}

	async function deleteProduct(id) {
		try {
			await http.delete('/api/products/' + id)
			showAlert('Produktas sėkmingai ištrintas', 'success')
			await setReloadTable(!reloadTable)
		} catch (error) {
			showAlert('Nepavyko ištrinti produkto', 'danger')
		}
	}

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5>Produktai</h5>
						<ButtonToolbar className="products-list__btn-toolbar-top">
							<Link to="/products/new">
								<Button variant="secondary" size="sm">
									<PlusIcon /> Kurti naują
								</Button>
							</Link>
						</ButtonToolbar>
					</Card.Title>
					<Table striped hover responsive>
						<thead>
							<tr>
								<th>Produkto pavadinimas</th>
								<th>Aprašymas</th>
								<th>Miestas</th>
								<th>Kategorija</th>
								<th>Veiksmai</th>
							</tr>
						</thead>
						<tbody>
							{products.map((item) => (
								<tr key={item._id}>
									<td>{item.name}</td>
									<td>{item.description}</td>
									<td>{item.city.name}</td>
									<td>{item.category.name}</td>
									<td>
										<ButtonGroup>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													deleteProduct(item._id)
												}}
											>
												<DeleteIcon />
											</a>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													history.push(`/products/${item._id}`)
												}}
											>
												<AccountEditIcon />
											</a>
										</ButtonGroup>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		</>
	)
}

export default ProductList
