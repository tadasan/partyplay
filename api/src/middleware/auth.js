const jwt = require('jsonwebtoken')
const cookie = require('cookie')
const User = require('../models/user')
const { jwtKey, sessionName } = require('../../config')
const logger = require('../logger')

const authentication = async (req, res, next) => {
	const token = cookie.parse(req.headers.cookie || '')[sessionName]
	if (token) {
		try {
			let decoded = jwt.verify(token, jwtKey)
			const now = new Date().getTime() / 1000
			if (now - decoded.iat > (decoded.exp - decoded.iat) / 2) {
				const refreshToken = jwt.sign({ _id: decoded._id, email: decoded.email }, jwtKey, { expiresIn: '2h' })
				res.setHeader('Set-Cookie', cookie.serialize(sessionName, refreshToken, { maxAge: 2 * 60 * 60, httpOnly: true, path: '/' }))
				decoded = jwt.verify(refreshToken, jwtKey)
			}
			const user = await User.findById(decoded._id).populate('userGroup', ['rights']).select(['-password', '-salt', '-__v']).lean()

			req.user = {
				_id: user._id,
				email: user.email,
				rights: user.userGroup.rights,
				iat: decoded.iat,
				exp: decoded.exp,
			}
			if (!user.googleAccessToken) {
				req.user.googleAccessGranted = false
			}

			return next()
		} catch (err) {
			logger.log({ level: 'warn', message: err.message})
			return res.sendStatus(401)
		}
	}
	res.sendStatus(401)
}

function authorization(right) {
	return async (req, res, next) => {
		if (req.user && req.user.rights) {
			if (req.user.rights.includes(right)) {
				return next()
			}
		}
		res.sendStatus(403)
	}
}


module.exports = {
	authentication,
	authorization,
}
