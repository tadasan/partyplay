import React, { useEffect, useState } from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import http from '../../http'
import { useForm, ErrorMessage, Controller } from 'react-hook-form'
import { useAlerts } from '../../alerts'
import Select from 'react-select'
import moment from 'moment'
import DatePicker, { registerLocale } from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import './styleFix.css'
import lt from "date-fns/locale/lt"
registerLocale("lt", lt)

function RentalNew() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const [productOptions, setProductOptions] = useState([])
    const [cityOptions, setCityOptions] = useState([])
    const [activeStep, setActiveStep] = useState(1)
    const [customerId, setCustomerId] = useState('')
    const [selectedProduct, setSelectedProduct] = useState('')
    const [busyDates, setBusyDates] = useState([])
    const { register, handleSubmit, errors, control, watch, setValue } = useForm({
        validateCriteriaMode: 'all',
    })
    const watchCity = watch('city')
    const watchProduct = watch('product')
    const watchStartDate = watch('startDate')
    const watchEndDate = watch('endDate')
	useEffect(() => { getCities() }, [])
    
    useEffect(() => {
        setValue('product', null)
        setValue('deposit', null)
        getProducts(watchCity)
    }, [watchCity])

    useEffect(() => {
        if (watchProduct) { getProduct(watchProduct.value) }
    }, [watchProduct])

    useEffect(() => {
        if (selectedProduct) { 
            setValue('deposit', selectedProduct.pricing.deposit) 
            getBusyDates(selectedProduct._id)
        }
    }, [selectedProduct])
    
    useEffect(() => {
        if(watchStartDate && watchEndDate && selectedProduct) {
            const fee = getFee(watchStartDate, watchEndDate, selectedProduct.pricing.fees)
            setValue('fee', fee)
        }
    }, [ watchStartDate, watchEndDate, selectedProduct])

    async function getBusyDates(productId) {
        try {
            const response = await http.get(`/api/rentals/products/${productId}/busy-dates`)
            filterBusyDates(response.data)
			setBusyDates(response.data)
		} catch (error) {}
    }


    function getFee(startDate, endDate, fees) {
        const days = moment(endDate).subtract(1, 's').diff(startDate, 'days')
        return fees[days]
    }

	async function getCities() {
		try {
			const response = await http.get('/api/cities')
			setCityOptions(
				response.data.map((city) => ({ value: city._id, label: city.name })),
			)
		} catch (error) {}
	}

	async function getProducts(city) {
		try {
            const response = await http.get(`/api/products?city=${city.value}`)
			setProductOptions(
				response.data.map((product) => ({
					value: product._id,
					label: product.name,
				})),
			)
		} catch (error) {}
    }

    async function getProduct(productId) {
        try {
            const response = await http.get(`/api/products/${productId}`)
            setSelectedProduct(response.data)
		} catch (error) {}
    }
    
    async function createCustomer(dto) {
		try {
            const response = await http.post('/api/customers/', dto)
            if (response.status === 201) {
                setCustomerId(response.headers.location.split('/').pop())
            }
            if (response.status === 200) {
                showAlert('Sistemoje rastas ir parinktas egzistuojantis naudotojas, kuriam priklauso jūsų nurodytas tel. numeris.', 'warning')
                setCustomerId(response.data._id)
            }
            setActiveStep(2)
		} catch (error) {
			showAlert('Klaida', 'danger')
		}
	}

	async function createRental(dto) {
		try {
			const response = await http.post('/api/rentals/', dto)
			showAlert('Nuomos rezervacija sėkmingai patvirtinta', 'success')
			const newRentalId = response.headers.location.split('/').pop()
			history.push(`/rentals/edit/${newRentalId}`)
		} catch (error) {
            if (error.response.data.data === 'RentalStartDayIsBusy') {
				return showAlert('Nuomos pradžios data jau užimta', 'danger')
            }
            if (error.response.data.data === 'RentalEndDayIsBusy') {
				return showAlert('Nuomos pabaigos data jau užimta', 'danger')
            }
            if (error.response.data.data === 'RentalEndDateBeforeStartDate') {
				return showAlert('Nuomos pabaiga turi būti vėliau pradžios', 'danger')
            }
            if (error.response.data.data === 'RentalStartDateBeforeNew') {
				return showAlert('Nuoma negali prasidėti praeityje', 'danger')
            }
            if (error.response.data.data === 'RentalEndDateBeforeNew') {
				return showAlert('Nuoma negali baigtis praeityje', 'danger')
			}
			showAlert('Klaida', 'danger')
		}
	}
    
    const submitCustomer = formData => {
        const dto = {
            name: formData.customerName,
            phone: formData.phone,
        }
        createCustomer(dto)
    }

	const submitRental = formData => {
		const dto = { 
            customer: customerId,
            city: formData.city.value,
            product: formData.product.value,
            startDate: formData.startDate,
            endDate: formData.endDate,
            deposit: parseInt(formData.deposit),
            fee: parseInt(formData.fee),
        }
		createRental(dto)
    }
    
    const step1_createCustomer = 
        <Form onSubmit={handleSubmit(submitCustomer)} autoComplete="off">
            <Form.Label className="font-weight-bold">Kliento duomenys</Form.Label>
            <Form.Group>
                <Form.Label>Telefono numeris <small>(+370)</small></Form.Label>
                <Form.Control
                    type="text"
                    name="phone"
                    ref={register({ 
                        required: 'Šis laukelis privalomas',
                        pattern: {
							value: /^\+370[0-9]{8}$/,
							message: 'Neteisingas telefono numerio formatas',
                        }
					})}
                />
                <ErrorMessage errors={errors} name="phone">
                    {({ messages }) => messages && Object.entries(messages).map(([type, message]) => (
                        <p className="error-m" key={type}>{message}</p>
                    ))}
                </ErrorMessage>
            </Form.Group> 
            <Form.Group>
                <Form.Label>Vardas Pavardė</Form.Label>
                <Form.Control
                    type="text"
                    name="customerName"
                    ref={register}
                />
            </Form.Group>
            <Form.Group>
                <Form.Label>El. paštas</Form.Label>
                <Form.Control
                    type="text"
                    name="email"
                    ref={register}
                />
            </Form.Group>
            <Button variant="secondary" type="submit">
                Toliau
            </Button>
        </Form>

    const step2_createRental =
        <Form onSubmit={handleSubmit(submitRental)} autoComplete="off">
            <Form.Label className="font-weight-bold">Rezervacijos duomenys</Form.Label>
            <Form.Group>
                <Form.Label>Miestas</Form.Label>
                <Controller
                    as={<Select />}
                    options={cityOptions} 
                    control={control}
                    rules={{ required: 'Šis laukelis privalomas' }}
                    onChange={([selected]) => selected}
                    name="city"
                    placeholder="pasirinkti"
                />
                <ErrorMessage errors={errors} name="city" as="p" className="error-m"/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Produktas</Form.Label>
                <Controller
                    as={<Select/>}
                    options={productOptions}
                    control={control}
                    rules={{ required: 'Šis laukelis privalomas' }}
                    onChange={([selected]) => selected}
                    name="product"
                    placeholder="pasirinkti"
                />
                <ErrorMessage errors={errors} name="product" as="p" className="error-m"/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Pradžios data</Form.Label>
                <br></br>
                <Controller
                    as={<DatePicker />}
                    dateFormat="yyyy-MM-dd HH:mm"
                    timeFormat="HH:mm"
                    showTimeSelect
                    control={control}
                    valueName="selected"                 
                    onChange={([selected]) => selected}
                    name="startDate"
                    rules={{ required: 'Šis laukelis privalomas' }}
                    placeholderText="pasirinkti"
                    className="form-control"
                    locale="lt"
                />
                <ErrorMessage errors={errors} name="startDate" as="p" className="error-m"/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Pabaigos data</Form.Label>
                <br></br>
                <Controller
                    as={<DatePicker autocomplete="off"/>}
                    control={control}
                    dateFormat="yyyy-MM-dd HH:mm"
                    timeFormat="HH:mm"
                    showTimeSelect
                    valueName="selected"                 
                    onChange={([selected]) => selected}
                    name="endDate"
                    rules={{ required: 'Šis laukelis privalomas' }}
                    placeholderText="pasirinkti"
                    className="form-control"
                    locale="lt"
                />
                <ErrorMessage errors={errors} name="endDate" as="p" className="error-m"/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Užstatas</Form.Label>
                <Form.Control
                    type="text"
                    name="deposit"
                    ref={register({ required: 'Šis laukelis privalomas' })}
                />
                <ErrorMessage errors={errors} name="deposit" as="p" className="error-m"/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Nuomos mokestis</Form.Label>
                <Form.Control
                    type="text"
                    name="fee"
                    ref={register({ required: 'Šis laukelis privalomas' })}
                />
                <ErrorMessage errors={errors} name="deposit" as="p" className="error-m"/>
            </Form.Group>
            <Button variant="secondary" type="submit">
				Patvirtinti
			</Button>
        </Form>

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5 className="font-weight-bold"> Nauja nuoma </h5>
					</Card.Title>
					<Card.Body>
                        {activeStep === 1 && step1_createCustomer}
                        {activeStep === 2 && step2_createRental}
					</Card.Body>
				</Card.Body>
			</Card>
		</>
	)
}

export default RentalNew
