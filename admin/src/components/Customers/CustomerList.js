import React, { useRef, useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import {
	Card,
	Table,
	ButtonGroup,
} from 'react-bootstrap'
import DeleteIcon from 'mdi-react/DeleteIcon'
import AccountEditIcon from 'mdi-react/AccountEditIcon'
import http from '../../http'
import { useAlerts } from '../../alerts'

function CustomerList() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const [customers, setCustomers] = useState([])
	const [reloadTable, setReloadTable] = useState(false)

	useEffect(() => {
		getCustomers()
	}, [reloadTable])

	async function getCustomers() {
		try {
			const response = await http.get('/api/customers')
			setCustomers(response.data)
		} catch (error) {}
	}

	async function deleteCustomer(id) {
		try {
			await http.delete('/api/customers/' + id)
			showAlert('Klientas sėkmingai ištrintas', 'success')
			await setReloadTable(!reloadTable)
		} catch (error) {
			showAlert('Nepavyko ištrinti kontakto', 'danger')
		}
	}

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5>Klientai</h5>
					</Card.Title>
					<Table striped hover responsive>
						<thead>
							<tr>
								<th>Tel. nr</th>
								<th>Vardas</th>
                                <th>El. paštas</th>
								<th>Veiksmai</th>
							</tr>
						</thead>
						<tbody>
							{customers.map((item) => (
								<tr key={item._id}>
									<td>{item.phone}</td>
                                    <td>{item.name}</td>
									<td>{item.email}</td>
									<td>
										<ButtonGroup>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													deleteCustomer(item._id)
												}}
											>
												<DeleteIcon />
											</a>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													history.push(`/customers/${item._id}`)
												}}
											>
												<AccountEditIcon />
											</a>
										</ButtonGroup>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		</>
	)
}

export default CustomerList
