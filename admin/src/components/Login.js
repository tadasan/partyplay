import React from 'react'
import { Col, Row, Card, Button, Form } from 'react-bootstrap'
import { useAuth } from '../auth'
import { useForm, ErrorMessage } from 'react-hook-form'
import { useAlerts } from '../alerts'
import http from '../http'

function Login() {
	const { showAlert } = useAlerts()
	const { loadSession } = useAuth()
	const { register, handleSubmit, errors } = useForm({
		validateCriteriaMode: 'all',
	})


	async function login(dto) {
		try {
			await http.post('/api/session', dto)
		} catch (error) {
			if (error.response.status === 401) {
				showAlert('Neteisingi prisijungimo duomenys', 'danger')
			}
		}
		loadSession()
	}

	const onSubmit = (formData) => {
		const dto = {
			email: formData.email,
			password: formData.password,
		}
		login(dto)
	}

	return (
		<>
			<div>
				<Row className="justify-content-center full-height-wrapper align-items-center">
					<Col md={4}>
						<Card className="shadow rounded-0">
							<Card.Header>
								<Card.Title>
									<h5 className="bold-text">Prisijungimas</h5>
								</Card.Title>
							</Card.Header>
							<Card.Body>
								<Form onSubmit={handleSubmit(onSubmit)}>
									<Form.Group>
										<Form.Label>El. paštas:</Form.Label>
										<Form.Control
											type="text"
											name="email"
											ref={register({
												required: 'Šis laukelis privalomas',
												pattern: {
													value: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
													message: 'Neteisingas el. pašto formatas',
												},
											})}
										/>
										<ErrorMessage errors={errors} name="email">
											{({ messages }) =>
												messages &&
												Object.entries(messages).map(([type, message]) => (
													<p className="error-m" key={type}>
														{message}
													</p>
												))
											}
										</ErrorMessage>
									</Form.Group>
									<Form.Group>
										<Form.Label>Slaptažodis:</Form.Label>
										<Form.Control
											type="password"
											name="password"
											ref={register({ required: 'Šis laukelis privalomas' })}
										/>
										<ErrorMessage
											errors={errors}
											name="password"
											as="p"
											className="error-m"
										/>
									</Form.Group>
									<Button
										className="btn-block"
										variant="secondary"
										type="submit"
									>
										Prisijungti
									</Button>
								</Form>
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</div>
		</>
	)
}

export default Login
