import React from 'react'

function Hero() {
	return (
		<section id='hero-img'>
			<div className='container'>
				<div className='hero-text d-none d-sm-block text-right mb-5 ml-auto'>
					<div className='rainbow-text js--wp-1'>
						<div className='rainbow-text' />
						<span className='letter'>P</span>
						<span className='letter'>A</span>
						<span className='letter'>R</span>
						<span className='letter'>T</span>
						<span className='letter'>Y</span>
					</div>
					<div className='rainbow-text js--wp-1'>
						<span className='letter'>P</span>
						<span className='letter'>L</span>
						<span className='letter'>A</span>
						<span className='letter'>Y</span>
					</div>
					<p className='ml-auto js--wp-2'>Pramogos laisvalaikiui ir verslui</p>
				</div>
			</div>
		</section>
	)
}

export default Hero
