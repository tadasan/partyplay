import React from 'react'
import { Link } from 'react-router-dom'

function CityList({ cities }) {
	return (
		<section id='cities' className='pt-4 text-center'>
			<div className='container'>
				<h3 className='mb-3'>Pasirinkite savo miestą</h3>
			</div>
			<div className='d-flex justify-content-center text-white animated pulse'>
				{cities.map(city =>
					<Link key={city._id} to={city.name}>
						<div className='py-4 shape shape-2 text-center m-1'>
							<h4 className='mb-0'>{city.name}</h4>
						</div>
					</Link>,
				)}
			</div>
		</section>
	)
}

export default CityList
