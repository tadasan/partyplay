import React, { useState, useEffect, useMemo } from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import { useParams, useHistory } from 'react-router-dom'
import http from '../../http'
import { useForm, ErrorMessage } from 'react-hook-form'
import { useAlerts } from '../../alerts'

function UserGroupEdit() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const { id: userGroupId } = useParams()
	const isNew = useMemo(() => userGroupId === 'new', [userGroupId])
	const [data, setData] = useState({ name: '', rights: [] })
	const [rightsOptions, setRightsOptions] = useState([])
	const { register, handleSubmit, errors, reset } = useForm({})

	useEffect(() => {
		if (userGroupId !== 'new') {
			getUserGroup(userGroupId)
		}
	}, [userGroupId])

	useEffect(() => {
		getRights()
	}, [])

	async function getUserGroup(id) {
		try {
			const response = await http.get(`/api/user-groups/${id}`)
			setData({
				name: response.data.name,
				rights: response.data.rights,
			})
			reset({
				name: response.data.name,
			})
		} catch (error) {}
	}

	async function getRights() {
		try {
			const response = await http.get('/api/user-groups/rights')
			const rights = response.data.map((right) => ({ right }))
			setRightsOptions(rights)
		} catch (error) {}
	}

	async function createUserGroup(dto) {
		try {
			const response = await http.post('/api/user-groups/', dto)
			showAlert('Naudotojų grupė sėkmingai sukurta', 'success')
			const newUserGroupId = response.headers.location.split('/').pop()
			history.push(`/user-groups/${newUserGroupId}`)
		} catch (error) {
			if (error.response.data.data === 'UserGroupNameIsAlreadyInUse') {
				showAlert('Naudotojų grupės vardas jau užimtas', 'danger')
			}
		}
	}

	async function editUserGroup(dto) {
		try {
			await http.put(`/api/user-groups/${userGroupId}`, dto)
			showAlert('Naudotojų grupė sėkmingai atnaujinta', 'success')
		} catch (error) {
			if (error.response.data.data === 'UserGroupNameIsAlreadyInUse') {
				showAlert('Naudotojų grupės vardas jau užimtas', 'danger')
			}
		}
	}

	const onSubmit = (formData) => {
		const dto = {
			name: formData.name,
			rights: data.rights,
		}
		isNew ? createUserGroup(dto) : editUserGroup(dto)
	}

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5>
							{isNew ? 'Nauja naudotojų grupė' : 'Redaguoti naudotojų grupę'}
						</h5>
					</Card.Title>
					<Card.Body>
						<Form onSubmit={handleSubmit(onSubmit)}>
							<Form.Group>
								<Form.Label>Pavadinimas</Form.Label>
								<Form.Control
									type="text"
									name="name"
									ref={register({ required: 'Šis laukelis privalomas' })}
								/>
								<ErrorMessage
									errors={errors}
									name="name"
									as={<p className="error-m" />}
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label>Teisės:</Form.Label>
								{rightsOptions.map((right) => (
									<Form.Check
										key={right.right}
										name={right.right}
										label={right.right}
										checked={data.rights.includes(right.right)}
										onChange={(e) => {
											setData({
												...data,
												rights: e.target.checked
													? [...data.rights].concat(right.right)
													: [...data.rights].filter((x) => x !== right.right),
											})
										}}
										ref={register}
									/>
								))}
							</Form.Group>
							<Button variant="secondary" type="submit">
								Patvirtinti
							</Button>
						</Form>
					</Card.Body>
				</Card.Body>
			</Card>
		</>
	)
}

export default UserGroupEdit
