const request = require('supertest')
const UserGroup = require('../../src/models/userGroup')
const User = require('../../src/models/user')
const mongoose = require('mongoose')
const { sessionName } = require('../../config')
const { removeAllCollections, getJwtToken, server } = require('../testUtil')

const userGroupObj1 = {
	name: 'name1',
	rights: [],
}

const userGroupObj2 = {
	name: 'name2',
	rights: [],
}

describe('/api/user-groups', () => {

	beforeAll(async () => {
		const url = 'mongodb://localhost:27017/userGroupsDb'
  		await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
		await removeAllCollections()
	})

	describe('GET /rights', () => {

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should return all rights', async () => {
			const res = await request(server)
				.get('/api/user-groups/rights')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsRead'])}`)

			expect(res.status).toBe(200)
		})
	})

	describe('GET /', () => {

		beforeEach(async () => {
			const userGroup1 = new UserGroup(userGroupObj1)
			await userGroup1.save()

			const userGroup2 = new UserGroup(userGroupObj2)
			await userGroup2.save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should return all user groups', async () => {
			const res = await request(server)
				.get('/api/user-groups/')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsRead'])}`)

			expect(res.status).toBe(200)
			expect(res.body.some(p => p.name === 'name1')).toBeTruthy()
			expect(res.body.some(p => p.name === 'name2')).toBeTruthy()
		})


		it('should throw 401 if jwt token is not provided', async () => {
			const res = await request(server)
				.get('/api/user-groups/')

			expect(res.status).toBe(401)
		})

		it('should throw 403 if UserGroupRead right is not provided', async () => {
			const res = await request(server)
				.get('/api/user-groups/')
				.set('Cookie', `${sessionName}=${await getJwtToken([])}`)

			expect(res.status).toBe(403)
		})
	})

	describe('GET /:id', () => {
		let userGroup

		beforeEach(async () => {
			userGroup = new UserGroup(userGroupObj1)
			await userGroup.save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should return UserGroup if valid ID is passed', async () => {
			const res = await request(server)
				.get(`/api/user-groups/${userGroup._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsRead'])}`)

			expect(res.status).toBe(200)
			expect(res.body).toHaveProperty('name', userGroup.name)
			expect(res.body).toHaveProperty('rules', userGroup.category)
		})

		it('should throw 400 if UserGroup ID is wrong', async () => {
			const res = await request(server)
				.get('/api/user-groups/123')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsRead'])}`)

			expect(res.status).toBe(400)
		})

		it('should throw 401 if jwt token is not provided', async () => {
			const res = await request(server)
				.get(`/api/user-groups/${userGroup._id}`)

			expect(res.status).toBe(401)
		})

		it('should throw 404 if no UserGroup was found with provided valid ID', async () => {
			randomId = mongoose.Types.ObjectId()
			const res = await request(server)
				.get(`/api/user-groups/${randomId}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsRead'])}`)

			expect(res.status).toBe(404)
		})
	})

	describe('POST /', () => {
		const userGroupDto = { ...userGroupObj1 }

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should save the UserGroup if DTO is valid', async () => {
			const res = await request(server)
				.post('/api/user-groups')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsWrite'])}`)
				.send(userGroupDto)

			const createdId = res.header.location.split('/').pop()
			const created = await UserGroup.findById(createdId)

			expect(res.status).toBe(201)
			expect(created).not.toBeNull()
		})

		it('should return 400 if name is missing', async () => {
			const res = await request(server)
				.post('/api/user-groups')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsWrite'])}`)
				.send({ ...userGroupDto, name: undefined })

			expect(res.status).toBe(400)
		})

		it('should return 400 if UserGroup with same name already exists', async () => {
			await new UserGroup(userGroupDto).save()

			const res = await request(server)
				.post('/api/user-groups')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsWrite'])}`)
				.send(userGroupDto)

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('UserGroupNameIsAlreadyInUse')
		})

		it('should throw 401 if jwt token is not provided', async () => {
			const res = await request(server)
				.post('/api/user-groups')
				.send(userGroupDto)

			expect(res.status).toBe(401)
		})

		it('should throw 403 if UserGroupWrite right is not provided', async () => {
			const res = await request(server)
				.post('/api/user-groups')
				.set('Cookie', `${sessionName}=${await getJwtToken([])}`)
				.send(userGroupDto)

			expect(res.status).toBe(403)
		})

	})

	describe('PUT /:id', () => {
		const userGroupDto = { ...userGroupObj2 }
		let userGroup

		beforeEach(async () => {
			userGroup = new UserGroup(userGroupObj1)
			await userGroup.save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should update UserGroup if DTO is valid', async () => {
			const res = await request(server)
				.put(`/api/user-groups/${userGroup._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsWrite'])}`)
				.send(userGroupDto)

			expect(res.status).toBe(204)

			const updated = await UserGroup.findById(userGroup._id)
			expect(updated.name).toBe(userGroupDto.name)
		})

		it('should return 400 if name is missing', async () => {
			const res = await request(server)
				.put(`/api/user-groups/${userGroup._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsWrite'])}`)
				.send({ userGroupDto, name: undefined })

			expect(res.status).toBe(400)
		})

		it('should return 400 if UserGroup with same name already exists', async () => {
			await new UserGroup({ name: 'same', password: 'randommm' }).save()

			const res = await request(server)
				.put(`/api/user-groups/${userGroup._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsWrite'])}`)
				.send({ ...userGroupDto, name: 'same' })

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('UserGroupNameIsAlreadyInUse')
		})

		it('should return 404 if product with the given id was not found', async () => {
			const randomId = mongoose.Types.ObjectId()

			const res = await request(server)
				.put(`/api/user-groups/${randomId}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsWrite'])}`)
				.send(userGroupDto)

			expect(res.status).toBe(404)
		})
	})

	describe('DELETE /:id', () => {
		let userGroup

		beforeEach(async () => {
			userGroup = new UserGroup(userGroupObj1)
			await userGroup.save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should delete the UserGroup if ID is valid', async () => {
			const res = await request(server)
				.delete(`/api/user-groups/${userGroup._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsDelete'])}`)

			expect(res.status).toBe(200)
			const deleted = await UserGroup.findById(userGroup._id)
			expect(deleted).toBeNull()
		})

		it('should return 400 if trying to delete Admin userGroup', async () => {
			userGroup = await new UserGroup({ name: 'Admin', rights: [] }).save()

			const res = await request(server)
				.delete(`/api/user-groups/${userGroup._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsDelete'])}`)

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('CanNotDeleteAdmin')
		})

		it('should return 400 if deleting UserGroup is used by some Users', async () => {
			const user = new User({ email: 'random@test.tt', userGroup: userGroup._id })
			await user.setPassword('randommmm')
			await user.save()

			const res = await request(server)
				.delete(`/api/user-groups/${userGroup._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsDelete'])}`)

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('UserGroupReferenceInUser')
		})

		it('should return 404 if no UserGroup with the given id was found', async () => {
			const id = mongoose.Types.ObjectId()

			const res = await request(server)
				.delete(`/api/user-groups/${id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UserGroupsDelete'])}`)

			expect(res.status).toBe(404)
		})

		it('should throw 403 if UserGroupDelete right is not provided', async () => {
			const res = await request(server)
				.delete(`/api/user-groups/${userGroup._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken([])}`)

			expect(res.status).toBe(403)
		})

	})
})
