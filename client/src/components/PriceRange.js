import React, { useState } from 'react'

function PriceRange({ pricing }) {
	const [day, selectDay] = useState(1)

	function humanizeDays(day) {

		if (day === 1) {
			return day + ' para'
		} else if (day < 10) {
			return day + ' paros'
		} else {
			return day + ' parų'
		}
	}

	return (
		<div className='range'>
			<div className='value'>
				<span id='days'>{humanizeDays(day)}</span>
				<div className='price'>
					<span id='price'>{pricing.fees[day-1]}</span>€
				</div>
			</div>
			<input 
				type='range' 
				min='1' 
				max={pricing.fees.length} 
				step='1' 
				value={day} 
				onChange={e => selectDay(e.target.value)} 
			/>
		</div>
	)
}

export default PriceRange
