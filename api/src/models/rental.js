const mongoose = require('mongoose')

const rentalSchema = new mongoose.Schema({
	customer: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Customer',
		required: true,
	},
	city: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'City',
		required: true,
	},
	product: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Product',
		required: true,
	},
	startDate: {
		type: Date,
		required: true,
	},
	endDate: {
		type: Date,
		required: true,
	},
	deposit: {
		type: Number,
		required: true,
	},
	fee: {
		type: Number,
		required: true,
	},
	status: {
		type: String,
		required: true,
	}
})

rentalSchema.statics.status = [
	'reserved',
	'canceled',
	'active',
	'ended',
]

const Rental = mongoose.model('Rental', rentalSchema)

module.exports = Rental
