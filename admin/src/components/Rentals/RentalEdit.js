import React, { useEffect, useMemo, useState } from 'react'
import { Card, Button, Form, Row, Col, ButtonGroup } from 'react-bootstrap'
import { useParams, useHistory } from 'react-router-dom'
import http from '../../http'
import { useForm, ErrorMessage, Controller } from 'react-hook-form'
import { useAlerts } from '../../alerts'
import Select from 'react-select'
import moment from 'moment'
import DatePicker, { registerLocale } from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import './styleFix.css'
import lt from "date-fns/locale/lt"
registerLocale("lt", lt)

function RentalNew() {
	const { showAlert } = useAlerts()
    const { id: rentalId } = useParams()
    const [rental, setRental] = useState()
    const { register, handleSubmit, errors, control, reset } = useForm({
        validateCriteriaMode: 'all',
    })

	useEffect(() => {
		if (rentalId !== 'new') {
			getRental(rentalId)
		}
	}, [rental])

	async function getRental(id) {
		try {
            const response = await http.get(`/api/rentals/${id}`)
			reset({
                customer: response.data.customer.phone,
                city: response.data.city.name,
                product: response.data.product.name,
                startDate: moment(response.data.startDate).toDate(),
                endDate: moment(response.data.endDate).toDate(),
                deposit: response.data.deposit,
				fee: response.data.fee,
            })
            setRental(response.data)
		} catch (error) {}
    }

    async function activateRental() {
        try {
            await http.post(`/api/rentals/${rentalId}/activated`)
            setRental({})
        } catch (error) {}
    }

    async function endRental() {
        try {
            await http.post(`/api/rentals/${rentalId}/ended`)
            setRental({})
        } catch (error) {}
    }

	async function editRental(dto) {
		try {
			await http.put(`/api/rentals/${rentalId}`, dto)
			showAlert('Nuoma sėkmingai atnaujinta', 'success')
		} catch (error) {
            if (error.response.data.data === 'RentalStartDayIsBusy') {
				return showAlert('Nuomos pradžios data jau užimta', 'danger')
            }
            if (error.response.data.data === 'RentalEndDayIsBusy') {
				return showAlert('Nuomos pabaigos data jau užimta', 'danger')
            }
            if (error.response.data.data === 'RentalEndDateBeforeStartDate') {
				return showAlert('Nuomos pabaiga turi būti vėliau pradžios', 'danger')
            }
            if (error.response.data.data === 'RentalStartDateBeforeNew') {
				return showAlert('Nuoma negali prasidėti praeityje', 'danger')
            }
            if (error.response.data.data === 'RentalEndDateBeforeNew') {
				return showAlert('Nuoma negali baigtis praeityje', 'danger')
			}
			showAlert('Klaida', 'danger')
		}
    }

	const submitRental = formData => {
		const dto = { 
            customer: rental.customer._id,
            city: rental.city._id,
            product: rental.product._id,
            startDate: formData.startDate,
            endDate: formData.endDate,
            deposit: parseInt(formData.deposit),
            fee: parseInt(formData.fee),
            status: rental.status,
        }
		editRental(dto)
    }

    if (rentalId === 'new') {
        return <></>
    }

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5 className="font-weight-bold"> Nuomos redagavimas </h5>
					</Card.Title>
					<Card.Body>
                    <Form onSubmit={handleSubmit(submitRental)} autoComplete="off">
                        <Form.Group>
                            <Form.Label>Klientas</Form.Label>
                            <Form.Control
                                disabled
                                type="text"
                                name="customer"
                                ref={register}
                            />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Miestas</Form.Label>
                            <Form.Control
                                disabled
                                type="text"
                                name="city"
                                ref={register}
                            />                  
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Produktas</Form.Label>
                            <Form.Control
                                disabled
                                type="text"
                                name="product"
                                ref={register}
                            />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Pradžios data</Form.Label>
                            <br></br>
                            <Controller
                                as={<DatePicker />}
                                dateFormat="yyyy-MM-dd HH:mm"
                                timeFormat="HH:mm"
                                showTimeSelect
                                control={control}
                                valueName="selected"                 
                                onChange={([selected]) => selected}
                                name="startDate"
                                rules={{ required: 'Šis laukelis privalomas' }}
                                placeholderText="pasirinkti"
                                className="form-control"
                                locale="lt"  
                                disabled={rental && rental.status !== 'reserved'}
                            />
                            <ErrorMessage errors={errors} name="startDate" as="p" className="error-m"/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Pabaigos data</Form.Label>
                            <br></br>
                            <Controller
                                as={<DatePicker autocomplete="off"/>}
                                control={control}
                                dateFormat="yyyy-MM-dd HH:mm"
                                timeFormat="HH:mm"
                                showTimeSelect
                                valueName="selected"                 
                                onChange={([selected]) => selected}
                                name="endDate"
                                rules={{ required: 'Šis laukelis privalomas' }}
                                placeholderText="pasirinkti"
                                className="form-control"
                                locale="lt"
                                disabled={rental && rental.status === 'ended'}
                            />
                            <ErrorMessage errors={errors} name="endDate" as="p" className="error-m"/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Užstatas</Form.Label>
                            <Form.Control
                                type="text"
                                name="deposit"
                                ref={register({ required: 'Šis laukelis privalomas' })}
                                disabled={rental && rental.status === 'ended'}
                            />
                            <ErrorMessage errors={errors} name="deposit" as="p" className="error-m"/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Nuomos mokestis</Form.Label>
                            <Form.Control
                                type="text"
                                name="fee"
                                ref={register({ required: 'Šis laukelis privalomas' })}
                                disabled={rental && rental.status === 'ended'}
                            />
                            <ErrorMessage errors={errors} name="deposit" as="p" className="error-m"/>
                        </Form.Group>
                        <Button variant="secondary" type="submit" className="mr-3" disabled={rental && rental.status === 'ended'} >
                            Atnaujinti
                        </Button>

                        { rental && rental.status === 'reserved' && 
                            <Button variant="primary" onClick={activateRental}> 
                                Žymėti nuomos pradžią 
                            </Button> 
                        }
                        { rental && rental.status === 'active' && 
                            <Button variant="primary" onClick={endRental}> 
                                Žymėti nuomos pabaigą 
                            </Button> 
                        }
                    </Form>
					</Card.Body>
				</Card.Body>
			</Card>
		</>
	)
}

export default RentalNew
