const mongoose = require('mongoose')

const customerSchema = new mongoose.Schema({
	name: {
		type: String,
	},
	phone: {
		type: String,
		required: true,
		unique: true,
	},
	email: {
		type: String,
	},
	googleResourceName: {
		type: String,
	}
})

const Customer = mongoose.model('Customer', customerSchema)

module.exports = Customer