const router = require('express').Router()
const { authentication } = require('./middleware/auth')

router.use('/session', require('./routes/session'))
router.use('/client', require('./routes/client'))
router.use('/files', require('./routes/files'))
router.use(authentication)
router.use('/users', require('./routes/users'))
router.use('/user-groups', require('./routes/userGroups'))
router.use('/cities', require('./routes/cities'))
router.use('/categories', require('./routes/categories'))
router.use('/products', require('./routes/products'))
router.use('/customers', require('./routes/customers'))
router.use('/rentals', require('./routes/rentals'))

module.exports = router
