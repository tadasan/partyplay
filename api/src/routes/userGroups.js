const router = require('express').Router()
const { authentication, authorization } = require('../middleware/auth')
const { validateJSON, validateObjectID, getPaging } = require('../util')
const { ValidationError } = require('../errors')
const UserGroup = require('../models/userGroup')
const User = require('../models/user')
const wrap = require('express-async-wrap')

const userGroupSchema = {
	type: 'object',
	properties: {
		name: {
			type: 'string',
		},
		rights: {
			type: 'array',
			items: {
				type: 'string',
				enum: UserGroup.rights,
			},
		},
	},
	required: ['name'],
}

function setUserGroupFields(userGroupEntity, dto) {
	userGroupEntity.name = dto.name
	userGroupEntity.rights = dto.rights
}

router.post('/', authorization('UserGroupsWrite'), wrap(async (req, res) => {
	validateJSON(req.body, userGroupSchema)

	if (await UserGroup.countDocuments({ name: req.body.name })) {
		throw new ValidationError('UserGroupNameIsAlreadyInUse')
	}

	const userGroup = new UserGroup()
	setUserGroupFields(userGroup, req.body)

	await userGroup.save()

	res.set('Location', `${req.baseUrl}/${userGroup._id}`)
	res.sendStatus(201)
}))

router.get('/', authorization('UserGroupsRead'), wrap(async (req, res) => {
	const { size, page } = getPaging(req.query)
	const userGroups = await UserGroup.find()
		.select(['-__v'])
		.limit(size)
		.skip(size * page)
		.lean()

	res.json(userGroups)
}))

router.get('/rights', authorization('UserGroupsRead'), wrap(async (req, res) => {
	const allRights = UserGroup.rights
	res.json(allRights)
}))

router.get('/:id', authorization('UserGroupsRead'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const userGroup = await UserGroup.findById(req.params.id).select(['-__v']).lean()

	if (!userGroup) {
		throw new Error('NotFound')
	}

	res.json(userGroup)
}))

router.put('/:id', authorization('UserGroupsWrite'), wrap(async (req, res) => {
	validateObjectID(req.params.id)
	validateJSON(req.body, userGroupSchema)


	const userGroup = await UserGroup.findById(req.params.id)
	if (!userGroup) {
		throw new Error('NotFound')
	}

	const userGroupCount = await UserGroup.countDocuments({ name: req.body.name, _id: { $ne: userGroup._id } })
	if (userGroupCount) {
		throw new ValidationError('UserGroupNameIsAlreadyInUse')
	}

	setUserGroupFields(userGroup, req.body)
	await userGroup.save()

	res.sendStatus(204)
}))

router.delete('/:id', authorization('UserGroupsDelete'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const userGroup = await UserGroup.findById(req.params.id)

	if (!userGroup) {
		throw new Error('NotFound')
	}

	if (await User.countDocuments({ userGroup: req.params.id })) {
		throw new ValidationError('UserGroupReferenceInUser')
	}

	if (userGroup.name !== 'Admin') {
		await userGroup.delete()
	} else {
		throw new ValidationError('CanNotDeleteAdmin')
	}

	res.sendStatus(200)
}))

module.exports = router
