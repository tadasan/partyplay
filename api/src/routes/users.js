const router = require('express').Router()
const { authentication, authorization } = require('../middleware/auth')
const wrap = require('express-async-wrap')
const { validateJSON, validateObjectID, getPaging } = require('../util')
const { ValidationError } = require('../errors')
const UserGroup = require('../models/userGroup')
const User = require('../models/user')
const { adminEmail } = require('../../config')

const userCreateSchema = {
	type: 'object',
	properties: {
		email: {
			type: 'string',
			format: 'email',
		},
		userGroup: {
			type: 'string',
			pattern: '^[0-9a-fA-F]{24}$',
		},
		firstName: {
			type: 'string',
		},
		lastName: {
			type: 'string',
		},
		password: {
			type: 'string',
		},
	},
	required: ['email', 'password', 'userGroup'],
}

const userUpdateSchema = {
	type: 'object',
	properties: {
		email: {
			type: 'string',
			format: 'email',
		},
		userGroup: {
			type: 'string',
			pattern: '^[0-9a-fA-F]{24}$',
		},
		name: {
			type: 'string',
		},
	},
	required: ['email', 'userGroup'],
}

async function setUserFields(userEntity, dto) {
	userEntity.email = dto.email
	userEntity.userGroup = dto.userGroup
	userEntity.name = dto.name
  	if (dto.password) { await userEntity.setPassword(dto.password) }
}

router.use(authentication)

router.post('/', authorization('UsersWrite'), wrap(async (req, res) => {
	validateJSON(req.body, userCreateSchema)

	if (await User.countDocuments({ email: req.body.email })) {
		throw new ValidationError('UserEmailAlreadyInUse')
	}

	if (!await UserGroup.countDocuments({ _id: req.body.userGroup })) {
		throw new ValidationError('UserGroupNotFound')
	}

	const user = new User()
	await setUserFields(user, req.body)
	await user.save()

	res.set('Location', `${req.baseUrl}/${user._id}`)
	res.sendStatus(201)
}))

router.get('/', authorization('UsersRead'), wrap(async (req, res) => {
	const { size, page } = getPaging(req.query)
	const users = await User.find()
		.select(['-password', '-__v', '-salt'])
		.limit(size)
		.skip(size * page)
		.lean()

	res.json(users)
}))

router.get('/:id', authorization('UsersRead'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const user = await User.findById(req.params.id)
		.select(['-password', '-__v', '-salt'])
		.populate('userGroup', ['-rights', '-__v'])
		.lean()

	if (!user) {
		throw new Error('NotFound')
	}

	res.json(user)
}))

router.put('/:id', authorization('UsersWrite'), wrap(async (req, res) => {
	validateObjectID(req.params.id)
	validateJSON(req.body, userUpdateSchema)

	const user = await User.findById(req.params.id)
	if (!user) {
		throw new Error('NotFound')
	}

	const userCount = await User.countDocuments({ email: req.body.email })
	if (userCount && user.email !== req.body.email) {
		throw new ValidationError('UserEmailAlreadyInUse')
	}

	if (!await UserGroup.countDocuments({ _id: req.body.userGroup })) {
		throw new ValidationError('UserGroupNotFound')
	}

	await setUserFields(user, req.body)
	await user.save()

	res.sendStatus(204)
}))

router.delete('/:id', authorization('UsersDelete'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const user = await User.findById(req.params.id).select(['-password', '-__v'])

	if (!user) {
		throw new Error('NotFound')
	}

	if (user.email === req.user.email) {
		throw new ValidationError('UserCanNotSelfDelete')
	} else if (user.email === adminEmail) {
		throw new ValidationError('UserCanNotDeleteAdmin')
	} else { await user.delete() }

	res.sendStatus(200)
}))

module.exports = router
