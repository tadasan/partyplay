import React, { useRef, useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import {
	Card,
	ButtonToolbar,
	Table,
	Button,
	ButtonGroup,
} from 'react-bootstrap'
import PlusIcon from 'mdi-react/PlusIcon'
import DeleteIcon from 'mdi-react/DeleteIcon'
import AccountEditIcon from 'mdi-react/AccountEditIcon'
import http from '../../http'
import { useAlerts } from '../../alerts'

function UserList() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const [users, setUsers] = useState([])
	const [reloadTable, setReloadTable] = useState(false)

	useEffect(() => {
		getUsers()
	}, [reloadTable])

	async function getUsers() {
		try {
			const response = await http.get('/api/users')
			setUsers(response.data)
		} catch (error) {}
	}

	async function deleteUser(id) {
		try {
			await http.delete('/api/users/' + id)
			showAlert('Naudotojas sėkmingai ištrintas', 'success')
			await setReloadTable(!reloadTable)
		} catch (error) {
			showAlert('Nepavyko ištrinti naudotojo', 'danger')
		}
	}

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5>Naudotojai</h5>
						<ButtonToolbar className="products-list__btn-toolbar-top">
							<Link to="/users/new">
								<Button variant="secondary" size="sm">
									<PlusIcon /> Kurti naują
								</Button>
							</Link>
						</ButtonToolbar>
					</Card.Title>
					<Table striped hover responsive>
						<thead>
							<tr>
								<th>El. paštas</th>
								<th>Vardas</th>
								<th>Veiksmai</th>
							</tr>
						</thead>
						<tbody>
							{users.map((item) => (
								<tr key={item._id}>
									<td>{item.email}</td>
									<td>{item.name}</td>
									<td>
										<ButtonGroup>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													deleteUser(item._id)
												}}
											>
												<DeleteIcon />
											</a>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													history.push(`/users/${item._id}`)
												}}
											>
												<AccountEditIcon />
											</a>
										</ButtonGroup>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		</>
	)
}

export default UserList
