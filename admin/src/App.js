import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Layout from './components/Layout'
import { ProvideAuth } from './auth'
import { ProvideAlerts } from './alerts'

function App() {
	return (
		<ProvideAlerts>
			<ProvideAuth>
				<Router>
					<Route exact component={Layout} />
				</Router>
			</ProvideAuth>
		</ProvideAlerts>
	)
}

export default App
