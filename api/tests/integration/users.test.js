const request = require('supertest')
const UserGroup = require('../../src/models/userGroup')
const User = require('../../src/models/user')
const mongoose = require('mongoose')
const { sessionName, adminEmail, jwtKey } = require('../../config')
const { removeAllCollections, getJwtToken, server } = require('../testUtil')
const jwt = require('jsonwebtoken')

describe('/api/users', () => {

	beforeAll(async () => {
		const url = 'mongodb://localhost:27017/usersDb'
		await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
		await removeAllCollections()
	})

	describe('GET /', () => {

		beforeEach(async () => {
			const userGroup1 = await new UserGroup({ name: 'UserGroup1', rights: [] })
			await userGroup1.save()

			const user1 = await new User({ email: 'usertest1@mail.com', userGroup: userGroup1._id })
			await user1.setPassword('randompass')
			await user1.save()

			const user2 = await new User({ email: 'usertest2@mail.com', userGroup: userGroup1._id })
			await user2.setPassword('randompass')
			await user2.save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should return all users', async () => {
			const res = await request(server)
				.get('/api/users/')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersRead'])}`)

			expect(res.status).toBe(200)
			expect(res.body.some(p => p.email === 'usertest1@mail.com')).toBeTruthy()
			expect(res.body.some(p => p.email === 'usertest1@mail.com')).toBeTruthy()
		})

		it('should throw 401 if jwt token is not provided', async () => {
			const res = await request(server)
				.get('/api/users/')

			expect(res.status).toBe(401)
		})

		it('should throw 403 if UsersRead right is not provided', async () => {
			const res = await request(server)
				.get('/api/users/')
				.set('Cookie', `${sessionName}=${await getJwtToken([])}`)

			expect(res.status).toBe(403)
		})
	})

	describe('GET /:id', () => {
		let user

		beforeEach(async () => {
			const userGroup1 = await new UserGroup({ name: 'UserGroup1', rights: [] })
			await userGroup1.save()

			user = await new User({ email: 'usertest1@mail.com', userGroup: userGroup1._id })
			await user.setPassword('randompass')
			await user.save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should return UserGroup if valid ID is passed', async () => {
			const res = await request(server)
				.get(`/api/users/${user._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersRead'])}`)

			expect(res.status).toBe(200)
			expect(res.body).toHaveProperty('email', user.email)
		})

		it('should throw 400 if UserGroup ID is not found', async () => {
			const res = await request(server)
				.get('/api/users/123')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersRead'])}`)

			expect(res.status).toBe(400)
		})

		it('should throw 401 if jwt token is not provided', async () => {
			const res = await request(server)
				.get(`/api/users/${user._id}`)

			expect(res.status).toBe(401)
		})

		it('should throw 404 if no UserGroup was found with provided valid ID', async () => {
			const randomId = mongoose.Types.ObjectId()
			const res = await request(server)
				.get(`/api/users/${randomId}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersRead'])}`)

			expect(res.status).toBe(404)
		})
	})

	describe('POST /', () => {
		const userDto = { email: 'random@random.lt', password: 'randommm' }
		let userGroup

		beforeEach(async () => {
			userGroup = await new UserGroup({ name: 'UserGroup1', rights: [] })
			await userGroup.save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should save the UserGroup if DTO is valid', async () => {
			const res = await request(server)
				.post('/api/users')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersWrite'])}`)
				.send({ ...userDto, userGroup: userGroup._id })

			const createdId = res.header.location.split('/').pop()
			const created = await User.findById(createdId)

			expect(res.status).toBe(201)
			expect(created).not.toBeNull()
		})

		it('should return 400 if name is missing', async () => {
			const res = await request(server)
				.post('/api/users')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersWrite'])}`)
				.send({ ...userDto, email: undefined, userGroup: userGroup._id })

			expect(res.status).toBe(400)
		})

		it('should return 400 if User with same name already exists', async () => {
			const jwt = await getJwtToken(['UsersWrite'])
			await request(server)
				.post('/api/users')
				.set('Cookie', `${sessionName}=${jwt}`)
				.send({ ...userDto, userGroup: userGroup._id })

			const res =	await request(server)
				.post('/api/users')
				.set('Cookie', `${sessionName}=${jwt}`)
				.send({ ...userDto, userGroup: userGroup._id })

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('UserEmailAlreadyInUse')
		})

		it('should return 400 if given userGroup does not exist', async () => {
			const randomId = mongoose.Types.ObjectId()

			const res =	await request(server)
				.post('/api/users')
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersWrite'])}`)
				.send({ ...userDto, userGroup: randomId })

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('UserGroupNotFound')
		})

		it('should throw 401 if jwt token is not provided', async () => {
			const res = await request(server)
				.post('/api/users')
				.send({ ...userDto, userGroup: userGroup._id })

			expect(res.status).toBe(401)
		})

		it('should throw 403 if UsersWrite right is not provided', async () => {
			const res = await request(server)
				.post('/api/users')
				.set('Cookie', `${sessionName}=${await getJwtToken([])}`)
				.send({ ...userDto, userGroup: userGroup._id })

			expect(res.status).toBe(403)
		})
	})

	describe('PUT /:id', () => {
		const userDto = { email: 'random2@random.lt', password: 'randommm' }
		let userGroup
		let user1

		beforeEach(async () => {
			userGroup = await new UserGroup({ name: 'UserGroup1', rights: [] })
			await userGroup.save()

			user1 = await new User({ email: 'random@random.lt', password: 'randommm', userGroup: userGroup._id })
			await user1.setPassword('randompass')
			await user1.save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should update UserGroup if DTO is valid', async () => {
			const res = await request(server)
				.put(`/api/users/${user1._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersWrite'])}`)
				.send({ ...userDto, userGroup: userGroup._id, password: undefined })

			expect(res.status).toBe(204)

			const updated = await User.findById(user1._id)
			expect(updated.email).toBe(userDto.email)
		})

		it('should return 400 if User with same email already exists', async () => {
			const jwt = await getJwtToken(['UsersWrite'])

			const user2 = await new User({ email: 'rand@random.lt', password: 'randommm', userGroup: userGroup._id })
			await user2.setPassword('randompass')
			await user2.save()

			const res =	await request(server)
				.put(`/api/users/${user2._id}`)
				.set('Cookie', `${sessionName}=${jwt}`)
				.send({ ...userDto, email: 'random@random.lt', userGroup: userGroup._id })

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('UserEmailAlreadyInUse')
		})

		it('should return 400 if email is missing', async () => {
			const res = await request(server)
				.put(`/api/users/${user1._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersWrite'])}`)
				.send({ ...userDto, userGroup: userGroup._id, email: undefined })

			expect(res.status).toBe(400)
		})

		it('should return 400 if given userGroup does not exist', async () => {
			const randomId = mongoose.Types.ObjectId()

			const res =	await request(server)
				.put(`/api/users/${user1._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersWrite'])}`)
				.send({ ...userDto, userGroup: randomId })

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('UserGroupNotFound')
		})

		it('should return 404 if product with the given id was not found', async () => {
			const randomId = mongoose.Types.ObjectId()

			const res = await request(server)
				.put(`/api/users/${randomId}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersWrite'])}`)
				.send({ ...userDto, userGroup: userGroup._id })

			expect(res.status).toBe(404)
		})
	})

	describe('DELETE /:id', () => {
		let userGroup
		let user1

		beforeEach(async () => {
			userGroup = await new UserGroup({ name: 'UserGroup1', rights: [] })
			await userGroup.save()
			user1 = await new User({ email: 'random@random.lt', password: 'randommm', userGroup: userGroup._id })
			await user1.setPassword('randompass')
			await user1.save()
		})

		afterEach(async () => {
			await removeAllCollections()
		})

		it('should delete the User if ID is valid', async () => {
			const res = await request(server)
				.delete(`/api/users/${user1._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersDelete'])}`)

			expect(res.status).toBe(200)
			const deleted = await User.findById(user1._id)
			expect(deleted).toBeNull()
		})

		it('should return 400 if given email belongs to Admin', async () => {
			const admin = await new User({ email: adminEmail, userGroup: userGroup._id })
			await admin.setPassword('randompass')
			await admin.save()

			const res = await request(server)
				.delete(`/api/users/${admin._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersDelete'])}`)

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('UserCanNotDeleteAdmin')
		})

		it('should return 400 if user is trying to delete itself', async () => {
			const token = await getJwtToken(['UsersDelete'])
			let decoded = jwt.verify(token, jwtKey)

			const res = await request(server)
				.delete(`/api/users/${decoded._id}`)
				.set('Cookie', `${sessionName}=${token}`)

			expect(res.status).toBe(400)
			expect(res.body.data).toBe('UserCanNotSelfDelete')
		})

		it('should return 404 if no User with the given id was found', async () => {
			id = mongoose.Types.ObjectId()

			const res = await request(server)
				.delete(`/api/users/${id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken(['UsersDelete'])}`)

			expect(res.status).toBe(404)
		})

		it('should throw 403 if UsersDelete right is not provided', async () => {
			const res = await request(server)
				.delete(`/api/users/${user1._id}`)
				.set('Cookie', `${sessionName}=${await getJwtToken([])}`)

			expect(res.status).toBe(403)
		})
	})
})
