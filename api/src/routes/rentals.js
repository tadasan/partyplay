const Rental = require('../models/rental')
const Customer = require('../models/customer')
const City = require('../models/city')
const Product = require('../models/product')
const { authorization } = require('../middleware/auth')
const { validateJSON, validateObjectID } = require('../util')
const { ValidationError } = require('../errors')
const wrap = require('express-async-wrap')
const router = require('express').Router()
const moment = require('moment')
const { createNewGoogleContact, updateGoogleContactToActive, updateGoogleContactToEnded } = require('../services/googleServices/contactsService')
const { createNewStartEvent, createNewEndEvent } = require('../services/googleServices/calendarsService')

const rentalSchema = {
	type: 'object',
	properties: {
		customer: {
			type: 'string',
			pattern: '^[0-9a-fA-F]{24}$',
		},
		city: {
			type: 'string',
			pattern: '^[0-9a-fA-F]{24}$',
		},
		product: {
			type: 'string',
			pattern: '^[0-9a-fA-F]{24}$',
		},
		startDate: {
			type: 'string',
			format: 'date-time',
		},
		endDate: {
			type: 'string',
			format: 'date-time',
		},
		deposit: {
			type: 'number'
		},
		fee: {
			type: 'number',
		},
	},
	required: ['customer','city', 'product', 'startDate', 'endDate', 'deposit', 'fee'],
}

function setRentalFields(rentalEntity, dto) {
	rentalEntity.customer = dto.customer
	rentalEntity.city = dto.city
	rentalEntity.product = dto.product
	rentalEntity.startDate = dto.startDate
	rentalEntity.endDate = dto.endDate
	rentalEntity.deposit = dto.deposit
	rentalEntity.fee = dto.fee
	rentalEntity.status = dto.status
}

router.get('/', authorization('RentalsRead'), wrap(async (req, res) => {
	const rentals = await Rental.find({status: { $nin: ['canceled', 'ended'] }})
		.select(['-__v'])
		.populate('customer')
		.populate('city')
		.populate('product')
		.lean()

	res.send(rentals)
}))

router.get('/products/:id/busy-dates', authorization('RentalsRead'), wrap(async (req,res) => {
	validateObjectID(req.params.id)

	if (!await Product.countDocuments({ _id: req.params.id })) {
		throw new ValidationError('ProductNotFound')
	}

	const busyDates = await Rental.find({ product: req.params.id }).select(['startDate', 'endDate', '-_id'])

	res.send(busyDates)
}))

router.get('/:id', authorization('RentalsRead'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const rental = await Rental.findById(req.params.id)
		.select(['-__v'])
		.populate('customer')
		.populate('city')
		.populate('product')
		.lean()

	if (!rental) {
		throw new Error('NotFound')
	}

	res.send(rental)
}))

function checkIfDatesAreValid(startDate, endDate) {
	if(moment(startDate).isAfter(moment(endDate))) {
		throw new ValidationError('RentalEndDateBeforeStartDate')
	}
	if(moment(startDate).isBefore(moment())) {
		throw new ValidationError('RentalStartDateBeforeNew')
	}
	if(moment(endDate).isBefore(moment())) {
		throw new ValidationError('RentalEndDateBeforeNew')
	}
}

async function checkIfDatesAreNotBusy(product, startDate, endDate) {
	const busyDates = await Rental.find({ product: product, status: { $in: ['active', 'reserved'] }})
		.select(['startDate', 'endDate'])

	busyDates.map(x => {
		if (moment(startDate).isBetween(moment(x.startDate), moment(x.endDate), null, '[]')) {
			throw new ValidationError('RentalStartDayIsBusy')
		}
		if (moment(endDate).isBetween(moment(x.startDate), moment(x.endDate))) {
			throw new ValidationError('RentalEndDayIsBusy')
		}
		if (moment(startDate).isSameOrBefore(moment(x.startDate)) && moment(endDate).isSameOrAfter(x.endDate)) {
			throw new ValidationError('RentalEndDayIsBusy')
		}
	})
}

router.post('/', authorization('RentalsWrite'), wrap(async (req, res) => {
	validateJSON(req.body, rentalSchema)

	const customer = await Customer.findOne({ _id: req.body.customer })
	if (!customer) {
		throw new ValidationError('CustomerNotFound')
	}

	if (!await City.countDocuments({ _id: req.body.city })) {
		throw new ValidationError('CityNotFound')
	}

	if (!await Product.countDocuments({ _id: req.body.product })) {
		throw new ValidationError('ProductNotFound')
	}

	checkIfDatesAreValid(req.body.startDate, req.body.endDate)
	await checkIfDatesAreNotBusy(req.body.product, req.body.startDate, req.body.endDate)

	const rental = new Rental()
	setRentalFields(rental, { ...req.body, status: 'reserved' })
	await rental.save()

	await createNewGoogleContact(rental._id, req.user._id)
	await createNewStartEvent(rental._id, req.user._id)

	res.set('Location', `${req.baseUrl}/${rental._id}`)
	res.sendStatus(201)
}))

router.post('/:id/activated', authorization('RentalsWrite'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const rental = await Rental.findById(req.params.id)
	if (!rental) {
		throw new Error('NotFound')
	}
	rental.status = 'active'
	rental.startDate = new Date()
	await rental.save()

	updateGoogleContactToActive(rental._id, req.user._id)
	await createNewEndEvent(rental._id, req.user._id)

	res.sendStatus(204)
}))

router.post('/:id/ended', authorization('RentalsWrite'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const rental = await Rental.findById(req.params.id)
	if (!rental) {
		throw new Error('NotFound')
	}
	rental.status = 'ended'
	rental.endDate = new Date()
	await rental.save()

	await updateGoogleContactToEnded(rental._id, req.user._id)

	res.sendStatus(204)
}))

router.put('/:id', authorization('RentalsWrite'), wrap(async (req, res) => {
	validateObjectID(req.params.id)
	validateJSON(req.body, rentalSchema)

	checkIfDatesAreValid(req.body.startDate, req.body.endDate)

	const rental = await Rental.findById(req.params.id)
	if (!rental) {
		throw new Error('NotFound')
	}

	setRentalFields(rental, req.body)
	await rental.save()
	res.sendStatus(204)
}))

router.delete('/:id', authorization('RentalsDelete'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const rental = await Rental.findById(req.params.id)

	if (!rental) {
		throw new Error('NotFound')
	}

	rental.status = 'canceled'
	await rental.save()
	res.sendStatus(204)
}))

module.exports = router
