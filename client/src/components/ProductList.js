import React from 'react'
import PriceRange from './PriceRange'
import { apiUrl } from '../config'

function ProductList({ products, scroll }) {

	return (
		<>
			<section className='p-5 gradient mb-4 js--wp-5'>
				<div className='container text-center text-white'>
					<h1>Pasirinkite produktą</h1>
				</div>
			</section>
			<section id='kaunas-vr' className='py-5 mt-4'>
				<div className='container text-center'>
					<div className='row justify-content-center'>
						{products.map(product =>
							<div className='col-md-6'>
								<div className='card text-center'>
									<img src={`${apiUrl}/api/files/${product.image}`} alt='' className='img-fluid card-img-top' />
									<div className='card-body p-3'>
										<h3 className='text-info'>{product.name}</h3>
										<p className='mb-0'>{product.description}</p>
										<PriceRange pricing={product.pricing} />
										<ul className='list-group text-left'>
											<li className='list-group-item list-group-item-info'> Į komplektą įeina: </li>
											{product.completeSet.map(x =>
												<li className='list-group-item'><i className='fas fa-check' />{x}</li>,
													)}
										</ul>
										<a className='btn btn-info btn-block btn-pulse mt-2 text-white scroll' onClick={scroll} data-offset='80'>Išsinuomoti!</a>
									</div>
								</div>
							</div>
						)}
					</div>
				</div>
			</section>
		</>
	)
}

export default ProductList
