const { google } = require('googleapis')
const moment = require('moment')
const { getRental } = require('../rentalService')
const { authorize } = require('./authService')

async function createNewStartEvent(rentalId, userId) {
	const auth = await authorize(userId)
    const calendar = google.calendar({ version: 'v3', auth })

	const rental = await getRental(rentalId)
    const startDate = moment(rental.startDate).subtract(30, 'min')
    const endDate = moment(rental.startDate).add(30, 'min')
	const summary = 'Perduoti ' + rental.product.name + ' ' + rental.city.name

    await calendar.events.insert({
        calendarId: 'primary',
        requestBody: {
            summary: summary,
            description: rental.customer.phone,
            start: {
                dateTime: startDate,
            },
            end: {
                dateTime: endDate,
            },
            reminders: {
                useDefault: false,
                overrides: [
                    { method: 'popup', minutes: 60 * 2 },
                    { method: 'email', minutes: 60 * 24 },
                ],
            }
        }
    })
}

async function createNewEndEvent(rentalId, userId) {
	const auth = await authorize(userId)
    const calendar = google.calendar({ version: 'v3', auth })

	const rental = await getRental(rentalId)
    const startDate = moment(rental.endDate).subtract(30, 'min')
    const endDate = moment(rental.endDate).add(30, 'min')
	const summary = 'Atsiimti ' + rental.product.name + ' ' + rental.city.name

    await calendar.events.insert({
        calendarId: 'primary',
        requestBody: {
            summary: summary,
            description: rental.customer.phone,
            start: {
                dateTime: startDate,
            },
            end: {
                dateTime: endDate,
            },
            reminders: {
                useDefault: false,
                overrides: [
                    { method: 'popup', minutes: 60 * 2 },
                    { method: 'email', minutes: 60 * 18 },
                ],
            },
        }
    })
}

module.exports = {
	createNewStartEvent,
	createNewEndEvent,
}