const Customer = require('../../models/customer')
const User = require('../../models/user')
const { google } = require('googleapis')
const { googleContactGroupName } = require('../../../config')
const moment = require('moment')
const { getRental } = require('../rentalService')
const { authorize } = require('./authService')

async function createNewGoogleContact(rentalId, userId) {
	const auth = await authorize(userId)
	const people = google.people({ version: 'v1', auth })

	const rental = await getRental(rentalId)

	const startDate = moment(rental.startDate).format('MM.DD')
	const contactName = rental.product.name.split(' ').map(x => x.trim()).join('') + startDate 

	const { data: newContact } = await people.people.createContact({
		requestBody: {
			names: [{
				honorificPrefix: 'rezervuota',
				honorificSuffix: rental.city.name,
				givenName: contactName,
			}],
			phoneNumbers: [{
				value: rental.customer.phone,
			}],
			memberships: [{
				contactGroupMembership: {
					contactGroupResourceName: await getContactGroupResourceName(userId),
				}
			}]
		}
	})

	const customer = await Customer.findById(rental.customer._id)
	customer.googleResourceName = newContact.resourceName
	await customer.save()
}

async function getContactGroupResourceName(userId) {
	const user = await User.findById(userId)
	return await user.googleContactGroupResourceName 
}

async function updateGoogleContactToActive(rentalId, userId) {
	const auth = await authorize(userId)
	const people = google.people({ version: 'v1', auth })

	const rental = await getRental(rentalId)

	const contact = await people.people.get({
		resourceName: rental.customer.googleResourceName,
		personFields: 'metadata',
	})

	const endDate = moment(rental.endDate).format('MM.DD')
	const contactName = rental.product.name.split(' ').map(x => x.trim()).join('') + endDate 
	await people.people.updateContact({
		resourceName: rental.customer.googleResourceName,
		updatePersonFields: 'names',
		requestBody: {
			etag: contact.data.etag,
			names: [{
				honorificPrefix: 'išnuomota',
				honorificSuffix: rental.city.name,
				givenName: contactName,
			}],
		}
	})
}

async function updateGoogleContactToEnded(rentalId, userId) {
	const auth = await authorize(userId)
	const people = google.people({ version: 'v1', auth })

	const rental = await getRental(rentalId)

	const contact = await people.people.get({
		resourceName: rental.customer.googleResourceName,
		personFields: 'metadata',
	})

	const endDate = moment(rental.endDate).format('MM.DD')
	const contactName = rental.product.name.split(' ').map(x => x.trim()).join('') + endDate 
	await people.people.updateContact({
		resourceName: rental.customer.googleResourceName,
		updatePersonFields: 'names',
		requestBody: {
			etag: contact.data.etag,
			names: [{
				honorificPrefix: 'užbaigta',
				honorificSuffix: rental.city.name,
				givenName: contactName,
			}],
		}
	})
}

async function initializeContactGroup(userId) {
	const user = await User.findById(userId)
	console.log('before auth')
	const auth = await authorize(user._id)
	console.log('after auth')
	const people = google.people({ version: 'v1', auth })
	const {data: newGroup} = await people.contactGroups.create({
		requestBody: {
			contactGroup: {
				name: googleContactGroupName,
			}
		}
	})
	user.googleContactGroupResourceName = newGroup.resourceName
	await user.save()
}


module.exports = {
	createNewGoogleContact,
	updateGoogleContactToActive,
	updateGoogleContactToEnded,
	initializeContactGroup,
}