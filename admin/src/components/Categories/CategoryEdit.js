import React, { useEffect, useMemo, useState } from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import { useParams, useHistory } from 'react-router-dom'
import http from '../../http'
import { useForm, ErrorMessage } from 'react-hook-form'
import { useAlerts } from '../../alerts'
import { apiUrl } from '../../../config'

function CategoryEdit() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const { id: categoryId } = useParams()
	const isNew = useMemo(() => categoryId === 'new', [categoryId])
	const { register, handleSubmit, errors, reset } = useForm({})
	const [imgId, setImgId] = useState('')

	useEffect(() => {
		register({ name: 'image', type: 'custom' })
		if (categoryId !== 'new') {
			getCategory(categoryId)
		}
	}, [categoryId])

	async function getCategory(id) {
		try {
			const response = await http.get(`/api/categories/${id}`)
			reset({
				name: response.data.name,
				description: response.data.description,
			})
			setImgId(response.data.image)
		} catch (error) {}
	}

	async function createCategory(dto) {
		try {
			const response = await http.post('/api/categories/', dto)
			showAlert('Kategorija sėkmingai sukurta', 'success')
			const newCategoryId = response.headers.location.split('/').pop()
			history.push(`/categories/${newCategoryId}`)
		} catch (error) {
			if (error.response.data.data === 'CategoryNameAlreadyInUse') {
				showAlert('Kategorijos vardas jau užimtas', 'danger')
			}
		}
	}

	async function editCategory(dto) {
		try {
			await http.put(`/api/categories/${categoryId}`, dto)
			showAlert('Kategorija sėkmingai atnaujinta', 'success')
		} catch (error) {
			if (error.response.data.data === 'CategoryNameAlreadyInUse') {
				showAlert('Kategorijos vardas jau užimtas', 'danger')
			}
		}
	}

	async function uploadFile(event) {
		const data = new FormData()
		const file = event.target.files[0]
		data.append('file', file)
		try {
			const res = await http.post('/api/files', data)
			setImgId(res.data[0]._id)
		} catch (error) {}
	}

	const onSubmit = (formData) => {
		const dto = {
			name: formData.name,
			description: formData.description,
			image: imgId,
		}
		isNew ? createCategory(dto) : editCategory(dto)
	}

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5>{isNew ? 'Nauja kategorija' : 'Kategorijos redagavimas'}</h5>
					</Card.Title>
					<Card.Body>
						<Form onSubmit={handleSubmit(onSubmit)}>
							<Form.Group>
								<Form.Label>Kategorijos pavadinimas</Form.Label>
								<Form.Control
									type="text"
									name="name"
									ref={register({ required: 'Šis laukelis privalomas' })}
								/>
								<ErrorMessage
									errors={errors}
									name="name"
									as="p"
									className="error-m"
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label>Aprašymas</Form.Label>
								<Form.Control
									as="textarea"
									rows="4"
									type="text"
									name="description"
									ref={register({ required: 'Šis laukelis privalomas' })}
								/>
								<ErrorMessage
									errors={errors}
									name="description"
									as="p"
									className="error-m"
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label>Nuotrauka</Form.Label>
								{imgId && (
									<div className="mb-3">
										<img
											className="img-width-150"
											src={`${apiUrl}/api/files/${imgId}`}
										/>
									</div>
								)}
								<Form.File id="formcheck-api-custom" custom>
									<Form.File.Input
										onChange={uploadFile}
										ref={register(
											isNew ? { required: 'Šis laukelis privalomas' } : '',
										)}
										name="image"
									/>
									<Form.File.Label data-browse="Pasirinkti">
										{imgId ? 'Pakeisti nuotrauką' : 'Pasirinkti nuotrauką'}
									</Form.File.Label>
								</Form.File>
								<ErrorMessage
									errors={errors}
									name="image"
									as="p"
									className="error-m"
								/>
							</Form.Group>

							<Button variant="secondary" type="submit">
								Patvirtinti
							</Button>
						</Form>
					</Card.Body>
				</Card.Body>
			</Card>
		</>
	)
}

export default CategoryEdit
