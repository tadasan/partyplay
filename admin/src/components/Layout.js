import React, { useState } from 'react'
import { Route, useHistory, useLocation } from 'react-router-dom'
import { Container, Row, Col, Nav } from 'react-bootstrap'
import Logo from '../logo.png'
import '../style/style.css'
import LogoutIcon from 'mdi-react/LogoutVariantIcon'
import AccountIcon from 'mdi-react/AccountIcon'
import AccountMultipleIcon from 'mdi-react/AccountMultipleIcon'
import CityIcon from 'mdi-react/CityIcon'
import ShapeIcon from 'mdi-react/ShapeIcon'
import FormatListBulletedSquare from 'mdi-react/FormatListBulletedSquareIcon'
import HandshakeIcon from 'mdi-react/HandshakeIcon'
import FaceIcon from 'mdi-react/FaceIcon'

import UserGroupList from './UserGroups/UserGroupList'
import UserGroupEdit from './UserGroups/UserGroupEdit'
import UserList from './Users/UserList'
import UserEdit from './Users/UserEdit'
import CityList from './Cities/CityList'
import CityEdit from './Cities/CityEdit'
import CategoryList from './Categories/CategoryList'
import CategoryEdit from './Categories/CategoryEdit'
import ProductList from './Products/ProductList'
import ProductEdit from './Products/ProductEdit'
import RentalList from './Rentals/RentalList'
import RentalNew from './Rentals/RentalNew'
import RentalEdit from './Rentals/RentalEdit'
import CustomerList from './Customers/CustomerList'
import http from '../http'
import queryString from 'query-string'
import Login from './Login'

import { useAuth } from '../auth'
import Alert from './Alert'

function Layout() {
	const { session, hasRight, removeSession, loadSession } = useAuth()
	const history = useHistory()
	const location = useLocation()
	const [url, setUrl] = useState('')
	const [activeLink, setActiveLink] = useState(location.pathname.split('/')[1])


	if (session && session.googleAccessGranted === false) {
		if(queryString.parse(location.search).code) {
			sendGoogleAccessVerificationCode(queryString.parse(location.search).code, loadSession)
			history.push('/')
		} else {
			getGoogleAccessVerificationLink()
			return <div>
				<p>
					Jūs dar nepatvirtinote savo google paskyros. Norėdami naudotis sistema, atverkite šią nuorodą, bei atlikite nurodytus veiksmus:
				</p>
				<a href={url}>NUORODA</a>
			</div>
		}
		
	}

	async function getGoogleAccessVerificationLink() {
		try {
			const response = await http.get(`/api/session/google/auth-url`)
			setUrl(response.data)
		} catch (error) {}
	}

	async function sendGoogleAccessVerificationCode(code, callback) {
		try {
			await http.post(`/api/session/google/validate-code`, { code })
			await callback()
		} catch (error) {}
	}

	return (
		<>
			<Alert />
			{session === 'LOADING' ? null : !session ? (
				<Login />
			) : (
				<div>
					<div className="top-bar w-100 sticky-top shadow bg-secondary text-white">
						<div className="mx-4 d-flex justify-content-end align-items-center">
							<a onClick={() => {}} className="mr-auto logo-text">
								<span className="d-flex align-items-center">
									<img src={Logo} className="logo" />
									<p className="mb-0 d-none d-md-inline-block">Party Play</p>
								</span>
							</a>
							<span>{session.email}</span>
							<a className="link-click" onClick={() => removeSession()}>
								<LogoutIcon className="ml-4" />
							</a>
						</div>
					</div>
					<Container fluid>
						<Row>
							<Col xs={2} id="sidebar-wrapper">
								<Nav
									className="col-2 d-block sidebar mt-5"
									onSelect={(selectedKey) => {
										history.push('/' + selectedKey)
										setActiveLink(selectedKey)
									}}
								>
									<Nav.Link
										className={ activeLink === 'users' ? 'active-sidebar-link' : '' }
										eventKey="users"
									>
										<AccountIcon />
										<span className="d-none d-md-inline-block">Naudotojai</span>
									</Nav.Link>
									<Nav.Link
										className={
											activeLink === 'user-groups' ? 'active-sidebar-link' : ''
										}
										eventKey="user-groups"
									>
										<AccountMultipleIcon />
										<span className="d-none d-md-inline-block">
											Naudotojų grupės
										</span>
									</Nav.Link>
									<Nav.Link
										className={ activeLink === 'cities' ? 'active-sidebar-link' : '' }
										eventKey="cities"
									>
										<CityIcon />
										<span className="d-none d-md-inline-block">Miestai</span>
									</Nav.Link>
									<Nav.Link
										className={ activeLink === 'categories' ? 'active-sidebar-link' : '' }
										eventKey="categories"
									>
										<FormatListBulletedSquare />
										<span className="d-none d-md-inline-block">
											Kategorijos
										</span>
									</Nav.Link>
									<Nav.Link
										className={ activeLink === 'products' ? 'active-sidebar-link' : '' }
										eventKey="products"
									>
										<ShapeIcon />
										<span className="d-none d-md-inline-block">Produktai</span>
									</Nav.Link>
									<Nav.Link
										className={ activeLink === 'rentals' ? 'active-sidebar-link' : '' }
										eventKey="rentals"
									>
										<HandshakeIcon />
										<span className="d-none d-md-inline-block">Nuomos</span>
									</Nav.Link>
									<Nav.Link
										className={ activeLink === 'customers' ? 'active-sidebar-link' : '' }
										eventKey="customers"
									>
										<FaceIcon />
										<span className="d-none d-md-inline-block">Klientai</span>
									</Nav.Link>
									
								</Nav>
							</Col>
							<Col xs={10} className="bg-light" id="page-content-wrapper">
								<div className="mt-3">
									<Route exact path="/user-groups" component={UserGroupList} />
									<Route path="/user-groups/:id" component={UserGroupEdit} />
									<Route exact path="/users" component={UserList} />
									<Route path="/users/:id" component={UserEdit} />
									<Route exact path="/cities" component={CityList} />
									<Route path="/cities/:id" component={CityEdit} />
									<Route exact path="/categories" component={CategoryList} />
									<Route path="/categories/:id" component={CategoryEdit} />
									<Route exact path="/products" component={ProductList} />
									<Route path="/products/:id" component={ProductEdit} />
									<Route exact path="/rentals" component={RentalList} />
									<Route exact path="/rentals/new" component={RentalNew} />
									<Route path="/rentals/edit/:id" component={RentalEdit} />
									<Route exact path="/customers" component={CustomerList} />
								</div>
							</Col>
						</Row>
					</Container>
				</div>
			)}
		</>
	)
}

export default Layout
