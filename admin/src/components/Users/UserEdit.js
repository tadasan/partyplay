import React, { useState, useEffect, useMemo } from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import { useParams, useHistory } from 'react-router-dom'
import http from '../../http'
import { useForm, Controller, ErrorMessage } from 'react-hook-form'
import Select from 'react-select'
import { useAlerts } from '../../alerts'

function UserEdit() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const { id: userId } = useParams()
	const isNew = useMemo(() => userId === 'new', [userId])
	const [userGroupsOptions, setUserGroupsOptions] = useState([])

	const { register, handleSubmit, errors, reset, control } = useForm({
		validateCriteriaMode: 'all',
	})

	useEffect(() => {
		getUserGroups()
	}, [])

	useEffect(() => {
		if (userId !== 'new') {
			getUser(userId)
		}
	}, [userId])

	async function getUser(id) {
		try {
			const response = await http.get(`/api/users/${id}`)
			reset({
				email: response.data.email,
				name: response.data.name,
				userGroup: {
					value: response.data.userGroup._id,
					label: response.data.userGroup.name,
				},
			})
		} catch (error) {}
	}

	async function getUserGroups() {
		try {
			const response = await http.get('/api/user-groups')
			setUserGroupsOptions(
				response.data.map((userGroup) => ({
					value: userGroup._id,
					label: userGroup.name,
				})),
			)
		} catch (error) {}
	}

	async function createUser(dto) {
		try {
			const response = await http.post('/api/users/', dto)
			showAlert('Naudotojas sėkmingai sukurtas', 'success')
			const newUserId = response.headers.location.split('/').pop()
			history.push(`/users/${newUserId}`)
		} catch (error) {
			if (error.response.data.data === 'UserEmailAlreadyInUse') {
				showAlert('Naudotojo el. paštas jau užimtas', 'danger')
			}
		}
	}

	async function editUser(dto) {
		try {
			await http.put(`/api/users/${userId}`, dto)
			showAlert('Naudotojas sėkmingai atnaujintas', 'success')
		} catch (error) {
			if (error.response.data.data === 'UserEmailAlreadyInUse') {
				showAlert('Naudotojo el. paštas jau užimtas', 'danger')
			}
		}
	}

	const onSubmit = (formData) => {
		const dto = {
			name: formData.name,
			email: formData.email,
			userGroup: formData.userGroup.value,
			password: isNew ? formData.password : undefined,
		}
		isNew ? createUser(dto) : editUser(dto)
	}

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5>{isNew ? 'Naujas naudotojas' : 'Naudotojo redagavimas'}</h5>
					</Card.Title>
					<Card.Body>
						<Form onSubmit={handleSubmit(onSubmit)}>
							<Form.Group>
								<Form.Label>El. Paštas</Form.Label>
								<Form.Control
									type="text"
									name="email"
									ref={register({
										required: 'Šis laukelis privalomas',
										pattern: {
											value: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
											message: 'Neteisingas el. pašto formatas',
										},
									})}
								/>
								<ErrorMessage errors={errors} name="email">
									{({ messages }) =>
										messages &&
										Object.entries(messages).map(([type, message]) => (
											<p className="error-m" key={type}>{message}</p>
										))
									}
								</ErrorMessage>
							</Form.Group>
							<Form.Group>
								<Form.Label>Vardas</Form.Label>
								<Form.Control type="text" name="name" ref={register} />
							</Form.Group>
							<Form.Group>
								<Form.Label>Naudotojų grupė</Form.Label>
								<Controller
									as={<Select options={userGroupsOptions} />}
									control={control}
									rules={{ required: 'Šis laukelis privalomas' }}
									onChange={([selected]) => selected}
									name="userGroup"
								/>
								<ErrorMessage
									errors={errors}
									name="userGroup"
									as="p"
									className="error-m"
								/>
							</Form.Group>
							{isNew && (
								<Form.Group>
									<Form.Label>Slaptažodis:</Form.Label>
									<Form.Control
										type="password"
										name="password"
										ref={register({ required: 'Šis laukelis privalomas' })}
									/>
									<ErrorMessage
										errors={errors}
										name="password"
										as="p"
										className="error-m"
									/>
								</Form.Group>
							)}
							<Button variant="secondary" type="submit">
								Patvirtinti
							</Button>
						</Form>
					</Card.Body>
				</Card.Body>
			</Card>
		</>
	)
}

export default UserEdit
