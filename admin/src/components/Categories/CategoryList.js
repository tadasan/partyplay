import React, { useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import {
	Card,
	ButtonToolbar,
	Table,
	Button,
	ButtonGroup,
} from 'react-bootstrap'
import PlusIcon from 'mdi-react/PlusIcon'
import DeleteIcon from 'mdi-react/DeleteIcon'
import AccountEditIcon from 'mdi-react/AccountEditIcon'
import http from '../../http'
import { useAlerts } from '../../alerts'

function CategoryList() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const [categories, setCategories] = useState([])
	const [reloadTable, setReloadTable] = useState(false)

	useEffect(() => {
		getCategories()
	}, [reloadTable])

	async function getCategories() {
		try {
			const response = await http.get('/api/categories')
			setCategories(response.data)
		} catch (error) {}
	}

	async function deleteCategory(id) {
		try {
			await http.delete('/api/categories/' + id)
			showAlert('Kategorija sėkmingai ištrinta', 'success')
			await setReloadTable(!reloadTable)
		} catch (error) {
			if (error.response.data.data === 'CategoryReferenceInProducts') {
				return showAlert(
					'Ištrinti nepavyko, nes yra produktų, priklausančių šiai kategorijai',
					'danger',
				)
			}
			showAlert('Nepavyko ištrinti kategorijos', 'danger')
		}
	}

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5>Kategorijos</h5>
						<ButtonToolbar className="products-list__btn-toolbar-top">
							<Link to="/categories/new">
								<Button variant="secondary" size="sm">
									<PlusIcon /> Kurti naują
								</Button>
							</Link>
						</ButtonToolbar>
					</Card.Title>
					<Table striped hover responsive>
						<thead>
							<tr>
								<th>Kategorijos pavadinimas</th>
								<th>Aprašymas</th>
								<th>Veiksmai</th>
							</tr>
						</thead>
						<tbody>
							{categories.map((item) => (
								<tr key={item._id}>
									<td>{item.name}</td>
									<td style={{ minWidth: 300 }}>{item.description}</td>
									<td>
										<ButtonGroup>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													deleteCategory(item._id)
												}}
											>
												<DeleteIcon />
											</a>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													history.push(`/categories/${item._id}`)
												}}
											>
												<AccountEditIcon />
											</a>
										</ButtonGroup>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		</>
	)
}

export default CategoryList
