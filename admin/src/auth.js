import React, { useContext, useState, useEffect, createContext } from 'react'
import PropTypes from 'prop-types'
import http from './http'

const AuthContext = createContext()

export function ProvideAuth({ children }) {
	const auth = useProvideAuth()
	return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>
}

ProvideAuth.propTypes = {
	children: PropTypes.any,
}

export const useAuth = () => useContext(AuthContext)

export function useProvideAuth() {
	const [session, setSession] = useState('LOADING')

	async function loadSession() {
		try {
			const res = await http.get('/api/session')
			setSession(res.data)
		} catch (error) {
			setSession(null)
		}
	}

	async function removeSession() {
		try {
			await http.delete('/api/session')
			setSession(null)
		} catch (error) {
			setSession(null)
		}
	}

	function hasRight(right) {
		const rights = right.split('|')
		return session && !!session.rights.find(r => rights.includes(r))
	}

	useEffect(() => {
		loadSession()
	}, [])

	return { session, loadSession, removeSession, hasRight }
}
