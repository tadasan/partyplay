class ValidationError extends Error {
	constructor(data) {
		super('ValidationError')
		this.data = data
		this.message = 'ValidationError'
	}
}

module.exports = { ValidationError }
