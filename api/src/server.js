const app = require('./app')
const { port } = require('../config')

app.listen(process.env.PORT || 4000, () => console.info('listening on port', port))
