const router = require('express').Router()
const { authorization } = require('../middleware/auth')
const wrap = require('express-async-wrap')
const { validateJSON, validateObjectID, getPaging } = require('../util')
const { ValidationError } = require('../errors')
const City = require('../models/city')
const Product = require('../models/product')

const citySchema = {
	type: 'object',
	properties: {
		name: {
			type: 'string',
		},
		address: {
			type: 'string',
		},
	},
	required: ['name', 'address'],
}

function setCityFields(cityEntity, dto) {
	cityEntity.name = dto.name
	cityEntity.address = dto.address
}

router.get('/', authorization('CitiesRead'), wrap(async (req, res) => {
	if (req && res && req.headers['return-total-count'] === 'true') {
		res.set('Total-Count', await City.countDocuments({}))
	}
	const { size, page } = getPaging(req.query)
	const cities = await City.find()
		.select(['-__v'])
		.limit(size)
		.skip(size * page)
		.lean()

	res.json(cities)
}))

router.get('/:id', authorization('CitiesRead'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const city = await City.findById(req.params.id)
		.select(['-__v'])
		.lean()

	if (!city) {
		throw new Error('NotFound')
	}

	res.json(city)
}))

router.post('/', authorization('CitiesWrite'), wrap(async (req, res) => {
	validateJSON(req.body, citySchema)

	if (await City.countDocuments({ name: req.body.name })) {
		throw new ValidationError('CityNameAlreadyInUse')
	}

	const city = new City()
	setCityFields(city, req.body)
	await city.save()

	res.set('Location', `${req.baseUrl}/${city._id}`)
	res.sendStatus(201)
}))

router.put('/:id', authorization('CitiesWrite'), wrap(async (req, res) => {
	validateObjectID(req.params.id)
	validateJSON(req.body, citySchema)

	const city = await City.findById(req.params.id)
	if (!city) {
		throw new Error('NotFound')
	}

	const cityCount = await City.countDocuments({ name: req.body.name })
	if (cityCount && req.body.name !== city.name) {
		throw new ValidationError('CityNameAlreadyInUse')
	}

	await setCityFields(city, req.body)
	await city.save()

	res.sendStatus(204)
}))

router.delete('/:id', authorization('CitiesDelete'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const city = await City.findById(req.params.id)

	if (!city) {
		throw new Error('NotFound')
	}

	if (await Product.countDocuments({ city: req.params.id })) {
		throw new ValidationError('CategoryReferenceInCities')
	}

	await city.delete()
	res.sendStatus(200)
}))

module.exports = router
