import React, { useEffect, useState, useRef } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import ProductList from './ProductList'
import { apiUrl } from '../config'

function CategoryList() {
	const [categories, setCategories] = useState([])
	const { cityName } = useParams()
	const [activeCategory, setActiveCategory] = useState('')

	const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop) 
	const myRef = useRef(null)
	const executeScroll = () => scrollToRef(myRef)

	useEffect(() => {
		getCategories()
	}, [])

	async function getCategories() {
		try {
			const response = await axios.get(`${apiUrl}/api/client/cities/${cityName}/categories`)
			setCategories(response.data)
		} catch (error) {}
	}

	return (
		<>
			{categories.length === 0 ? <div></div> : <>
				<section id='products' className='py-5'>
					<div className='container text-center'>
						<h2 className='mb-5'>Pasirinkite kategoriją</h2>
						<div className='row justify-content-center js--wp-4'>
							{categories.map(category =>
								<div className='col-md-4'>
									<a key={category._id} onClick={() => setActiveCategory(category)} className='scroll' data-offset='250'>
										<div className='card shadow border-0 rounded-0'>
											<img src={`${apiUrl}/api/files/${category.image}`} alt='' className='card-img-top img-fluid' />
											<div className='card-body text-center p-3'>
												<div className='card-title'>
													<h3 className='text-info'>{category.name}</h3>
												</div>
												<div className='card-text'>
													{category.description}
												</div>
											</div>
										</div>
									</a>
								</div>
							)}
						</div>
					</div>
				</section>
				{activeCategory && <ProductList products={activeCategory.products} scroll={executeScroll}/>}
				<section id="contact" class="p-5 bg-dark text-white">
            		<div class="container" ref={myRef}>
                		<h1 class="text-center mb-4">Susisiekime</h1>
                		<hr/>
               			<div class="row">
							<div class="col-md-4">
								<h3 class="text-info">Apie mus</h3>
								<p>
								Party Play! Teikiame žaidimų konsolių, virtualios realybės akinių ir projektorių nuomos paslaugas Kaune ir Klaipėdoje.
								</p>
							</div>
							<div class="col-md-4">
								<h3 class="text-info">Kontaktai</h3>
								<p>
								Tel.nr.:<br/>
								+370 621 14494 - Tadas Andriuška<br/><br/>

								Elektroninis paštas:<br/>
								info@partyplay.lt
								</p>
							</div>
							<div class="col-md-4">
								<h3 class="text-info">Adresas</h3>
								<p>
									Nemuno g. 26, Kaunas<br/>
								<br/>
								Prieš atvykstant būtina susiderinti iš anksto</p>
								{/* <h3 class="text-info">Sekite mus</h3>
								<a href="https://www.facebook.com/PartyPlayNuoma/" target="blank"><i class="fab fa-facebook fa-2x mr-2"></i></a>
								<a href="https://www.instagram.com/PartyPlayNuoma/" target="blank"><i class="fab fa-instagram fa-2x"></i></a> */}
							</div>
						</div>
					</div>
				</section>

				<footer id="main-footer" class="text-center p-4">
					<div class="row">
						<div class="col">
							<div class="container text-white">
								<p class="mb-0 text-gradient">Copyright &copy; <span id="year"></span> PartyPlay.lt</p>
							</div>
						</div>
					</div>
				</footer>
			</>}
		</>
	)
}

export default CategoryList
