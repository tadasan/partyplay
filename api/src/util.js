const Validator = require('jsonschema').Validator
const { ValidationError } = require('./errors')

function validateJSON(object, schema) {
	const v = new Validator()
	const validationResult = v.validate(object, schema)

	if (validationResult.errors.length > 0) {
		throw new ValidationError(validationResult.errors.join(', '))
	}
}

function validateObjectID(id) {
	const schemaObjectID = {
		type: 'string',
		pattern: '^[0-9a-fA-F]{24}$',
	}
	validateJSON(id, schemaObjectID)
}

function getPaging(query) {
	const size = Math.min(Math.max(query.size | 0 || 10, 1), 100)
	const page = query.page | 0 || 0
	return { size, page }
}

module.exports = {
	validateJSON,
	validateObjectID,
	getPaging,
}
