import React, { useEffect, useMemo, useState } from 'react'
import { Card, Button, Form, Row, Col } from 'react-bootstrap'
import { useParams, useHistory } from 'react-router-dom'
import http from '../../http'
import { useForm, ErrorMessage, Controller } from 'react-hook-form'
import { useAlerts } from '../../alerts'
import Select from 'react-select'
import { apiUrl } from '../../../config'

function ProductEdit() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const { id: productId } = useParams()
	const isNew = useMemo(() => productId === 'new', [productId])
	const [categoryOptions, setCategoryOptions] = useState([])
	const [cityOptions, setCityOptions] = useState([])
	const [imgId, setImgId] = useState('')

	const { register, handleSubmit, errors, reset, control } = useForm({})

	useEffect(() => {
		getCategories()
		getCities()
	}, [])

	useEffect(() => {
		if (productId !== 'new') {
			getProduct(productId)
		}
	}, [productId])

	async function getProduct(id) {
		try {
			const response = await http.get(`/api/products/${id}`)
			reset({
				name: response.data.name,
				description: response.data.description,
				deposit: response.data.pricing.deposit,
				fees: feesToString(response.data.pricing.fees),
				completeSet: response.data.completeSet.join('\n'),
				category: {
					value: response.data.category._id,
					label: response.data.category.name,
				},
				city: { value: response.data.city._id, label: response.data.city.name },
			})
			setImgId(response.data.image)
		} catch (error) {}
	}

	async function getCities() {
		try {
			const response = await http.get('/api/cities')
			setCityOptions(
				response.data.map((city) => ({ value: city._id, label: city.name })),
			)
		} catch (error) {}
	}

	async function getCategories() {
		try {
			const response = await http.get('/api/categories')
			setCategoryOptions(
				response.data.map((category) => ({
					value: category._id,
					label: category.name,
				})),
			)
		} catch (error) {}
	}

	async function createProduct(dto) {
		try {
			const response = await http.post('/api/products/', dto)
			showAlert('Produktas sėkmingai sukurtas', 'success')
			const newProductId = response.headers.location.split('/').pop()
			history.push(`/products/${newProductId}`)
		} catch (error) {
			showAlert('Klaida', 'danger')
		}
	}

	async function editProduct(dto) {
		try {
			await http.put(`/api/products/${productId}`, dto)
			showAlert('Produktas sėkmingai atnaujintas', 'success')
		} catch (error) {
			showAlert('Klaida', 'danger')
		}
	}

	async function uploadFile(event) {
		const data = new FormData()
		const file = event.target.files[0]
		data.append('file', file)
		try {
			const res = await http.post('/api/files', data)
			setImgId(res.data[0]._id)
		} catch (error) {
			showAlert('Klaida', 'danger')
		}
	}

	const feesStringToArray = (feesString) => {
		try {
			const fees = feesString.split(';')
			if (fees.length >= 7) { 
				return fees
			} else { throw Error() } 
		} catch (error) {
			showAlert('Blogas kainų formatavimas', 'danger')
		}
	}

	const feesToString = (fees) => {
		return fees.join(';')
	}

	const onSubmit = (formData) => {
		const fees = feesStringToArray(formData.fees)
		const dto = {
			name: formData.name,
			description: formData.description,
			pricing: {
				deposit: parseInt(formData.deposit),
				fees: fees,
			},
			completeSet: formData.completeSet.split(/\n/),
			category: formData.category.value,
			city: formData.city.value,
			image: imgId,
		}
		if (fees) {
			isNew ? createProduct(dto) : editProduct(dto)
		}
	}

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5 className="font-weight-bold">
							{isNew ? 'Naujas produktas' : 'Produkto redagavimas'}
						</h5>
					</Card.Title>
					<Card.Body>
						<Form onSubmit={handleSubmit(onSubmit)}>
							<Form.Group>
								<Form.Label className="font-weight-bold">
									Produkto pavadinimas
								</Form.Label>
								<Form.Control
									type="text"
									name="name"
									ref={register({ required: 'Šis laukelis privalomas' })}
								/>
								<ErrorMessage
									errors={errors}
									name="name"
									as="p"
									className="error-m"
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label className="font-weight-bold">Kainynas</Form.Label>
								<Form.Group as={Row}>
									<Form.Label column sm={2}>
										Užstatas
									</Form.Label>
									<Col sm={10}>
										<Form.Control
											type="text"
											name="deposit"
											ref={register({ required: 'Šis laukelis privalomas' })}
										/>
										<ErrorMessage
											errors={errors}
											name="deposit"
											as="p"
											className="error-m"
										/>
									</Col>
								</Form.Group>
								<Form.Group as={Row}>
									<Form.Label column sm={2}>
										Kainos <br />
										<small>(dienų skaičius : kaina)</small>
									</Form.Label>
									<Col sm={10}>
										<Form.Control
											as="textarea"
											rows="4"
											type="text"
											name="fees"
											ref={register({ required: 'Šis laukelis privalomas' })}
										/>
										<ErrorMessage
											errors={errors}
											name="fees"
											as="p"
											className="error-m"
										/>
									</Col>
								</Form.Group>
							</Form.Group>
							<Form.Group>
								<Form.Label className="font-weight-bold">Aprašymas</Form.Label>
								<Form.Control
									as="textarea"
									rows="4"
									type="text"
									name="description"
									ref={register({ required: 'Šis laukelis privalomas' })}
								/>
								<ErrorMessage
									errors={errors}
									name="description"
									as="p"
									className="error-m"
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label className="font-weight-bold">
									Komplektacija (kiekvienam vienetui - nauja eilutė)
								</Form.Label>
								<Form.Control
									as="textarea"
									rows="4"
									type="text"
									name="completeSet"
									ref={register({ required: 'Šis laukelis privalomas' })}
								/>
								<ErrorMessage
									errors={errors}
									name="completeSet"
									as="p"
									className="error-m"
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label className="font-weight-bold">Kategorija</Form.Label>
								<Controller
									as={<Select options={categoryOptions} className="on-topper" />}
									control={control}
									rules={{ required: 'Šis laukelis privalomas' }}
									onChange={([selected]) => selected}
									name="category"
								/>
								<ErrorMessage
									errors={errors}
									name="category"
									as="p"
									className="error-m"
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label className="font-weight-bold">Miestas</Form.Label>
								<Controller
									as={<Select options={cityOptions} className="on-top" />}
									control={control}
									rules={{ required: 'Šis laukelis privalomas' }}
									onChange={([selected]) => selected}
									name="city"
								/>
								<ErrorMessage
									errors={errors}
									name="city"
									as="p"
									className="error-m"
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label className="font-weight-bold">Nuotrauka</Form.Label>
								{imgId && (
									<div className="mb-3">
										<img
											className="img-width-150"
											src={`${apiUrl}/api/files/${imgId}`}
										/>
									</div>
								)}
								<Form.File id="formcheck-api-custom" custom>
									<Form.File.Input
										onChange={uploadFile}
										ref={register(
											isNew ? { required: 'Šis laukelis privalomas' } : '',
										)}
										name="image"
									/>
									<Form.File.Label data-browse="Pasirinkti">
										{imgId ? 'Pakeisti nuotrauką' : 'Pasirinkti nuotrauką'}
									</Form.File.Label>
								</Form.File>
								<ErrorMessage
									errors={errors}
									name="image"
									as="p"
									className="error-m"
								/>
							</Form.Group>

							<Button variant="secondary" type="submit">
								Patvirtinti
							</Button>
						</Form>
					</Card.Body>
				</Card.Body>
			</Card>
		</>
	)
}

export default ProductEdit
