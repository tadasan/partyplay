import React from 'react'
import PropTypes from 'prop-types'
import AlertOutlineIcon from 'mdi-react/AlertOutlineIcon'
import StarOutlineIcon from 'mdi-react/StarOutlineIcon'
import { Alert as BootstrapAlert } from 'react-bootstrap'

import { useAlerts } from '../alerts'

function Alert() {

	const getAlertIcon = variant => {
		switch (variant) {
			case 'success': return <StarOutlineIcon />
			case 'danger': return <AlertOutlineIcon />
			case 'warning': return <AlertOutlineIcon />
		}
	}

	const { alert } = useAlerts()
	return (
		<div className='fixed-bottom alert-box ml-auto'>
			<BootstrapAlert variant={alert.variant} show={alert.show} className='ml-3 mr-3 d-flex align-items-center'>
				{getAlertIcon(alert.variant)}
				<span className='ml-2'>{alert.text}</span>
			</BootstrapAlert>
		</div>
	)
}

Alert.propTypes = {
	alertState: PropTypes.object,
}

export default Alert
