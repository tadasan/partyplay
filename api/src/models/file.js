const mongoose = require('mongoose')

const fileSchema = new mongoose.Schema({
	buffer: Buffer,
	filename: String,
	contentType: String,
})

const File = mongoose.model('File', fileSchema)

module.exports = File
