import React, { useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import {
	Card,
	ButtonToolbar,
	Table,
	Button,
	ButtonGroup,
} from 'react-bootstrap'
import PlusIcon from 'mdi-react/PlusIcon'
import DeleteIcon from 'mdi-react/DeleteIcon'
import AccountEditIcon from 'mdi-react/AccountEditIcon'
import http from '../../http'
import { useAlerts } from '../../alerts'

function UserGroupList() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const [userGroups, setUserGroups] = useState([])
	const [reloadTable, setReloadTable] = useState(false)

	useEffect(() => {
		getUserGroups()
	}, [reloadTable])

	async function getUserGroups() {
		try {
			const response = await http.get('/api/user-groups')
			setUserGroups(response.data)
		} catch (error) {}
	}

	async function deleteUserGroup(id) {
		try {
			await http.delete('/api/user-groups/' + id)
			showAlert('Naudotojų grupė sėkmingai ištrinta', 'success')
			await setReloadTable(!reloadTable)
		} catch (error) {
			if (error.response.data.data === 'UserGroupReferenceInUser') {
				return showAlert(
					'Ištrinti nepavyko, nes yra naudotojų, priklausančių šiai naudotojų grupei',
					'danger',
				)
			}
			showAlert('Nepavyko ištrinti naudotojų grupės', 'danger')
		}
	}

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5>Naudotojų grupės</h5>
						<ButtonToolbar className="products-list__btn-toolbar-top">
							<Link to="/user-groups/new">
								<Button variant="secondary" size="sm">
									<PlusIcon /> Kurti naują
								</Button>
							</Link>
						</ButtonToolbar>
					</Card.Title>
					<Table striped hover responsive>
						<thead>
							<tr>
								<th>Naudotojų grupės vardas</th>
								<th>Veiksmai</th>
							</tr>
						</thead>
						<tbody>
							{userGroups.map((item) => (
								<tr key={item._id}>
									<td>{item.name}</td>
									<td>
										<ButtonGroup>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													deleteUserGroup(item._id)
												}}
											>
												<DeleteIcon />
											</a>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													history.push(`/user-groups/${item._id}`)
												}}
											>
												<AccountEditIcon />
											</a>
										</ButtonGroup>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		</>
	)
}

export default UserGroupList
