const router = require('express').Router()
const { authentication, authorization } = require('../middleware/auth')
const wrap = require('express-async-wrap')
const { validateJSON, validateObjectID, getPaging } = require('../util')
const { ValidationError } = require('../errors')
const Category = require('../models/category')
const Product = require('../models/product')

const categorySchema = {
	type: 'object',
	properties: {
		name: {
			type: 'string',
		},
		description: {
			type: 'string',
		},
		image: {
			type: 'string',
			pattern: '^[0-9a-fA-F]{24}$',
		},
	},
	required: ['name', 'description'],
}

function setCategoryFields(categoryEntity, dto) {
	categoryEntity.name = dto.name
	categoryEntity.description = dto.description
	if (dto.image) { categoryEntity.image = dto.image }
}

router.get('/', wrap(async (req, res) => {
	if (req && res && req.headers['return-total-count'] === 'true') {
		res.set('Total-Count', await Category.countDocuments({}))
	}
	const { size, page } = getPaging(req.query)
	const categories = await Category.find()
		.select(['-__v'])
		.limit(size)
		.skip(size * page)
		.lean()

	res.json(categories)
}))

router.get('/:id', wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const category = await Category.findById(req.params.id)
		.select(['-__v'])
		.lean()

	if (!category) {
		throw new Error('NotFound')
	}

	res.json(category)
}))

router.use(authentication)

router.post('/', authorization('CategoriesWrite'), wrap(async (req, res) => {
	validateJSON(req.body, categorySchema)

	if (await Category.countDocuments({ name: req.body.name })) {
		throw new ValidationError('CategoryNameAlreadyInUse')
	}

	const category = new Category()
	setCategoryFields(category, req.body)
	await category.save()

	res.set('Location', `${req.baseUrl}/${category._id}`)
	res.sendStatus(201)
}))

router.put('/:id', authorization('CategoriesWrite'), wrap(async (req, res) => {
	validateObjectID(req.params.id)
	validateJSON(req.body, categorySchema)

	const category = await Category.findById(req.params.id)
	if (!category) {
		throw new Error('NotFound')
	}

	const categoryCount = await Category.countDocuments({ name: req.body.name })
	if (categoryCount && req.body.name !== category.name) {
		throw new ValidationError('CategoryNameAlreadyInUse')
	}

	await setCategoryFields(category, req.body)
	await category.save()

	res.sendStatus(204)
}))

router.delete('/:id', authorization('CategoriesDelete'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const category = await Category.findById(req.params.id)

	if (!category) {
		throw new Error('NotFound')
	}

	if (await Product.countDocuments({ category: req.params.id })) {
		throw new ValidationError('CategoryReferenceInProducts')
	}

	await category.delete()
	res.sendStatus(200)
}))

module.exports = router
