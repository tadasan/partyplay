const router = require('express').Router()
const { authorization } = require('../middleware/auth')
const wrap = require('express-async-wrap')
const Customer = require('../models/customer')
const { validateJSON, validateObjectID } = require('../util')

const customerSchema = {
	type: 'object',
	properties: {
		name: {
			type: 'string',
		},
		phone: {
			type: 'string', 
			pattern: /^\+370[0-9]{8}$/, 
		},
		email: {
			type: 'string',
			format: 'email',
		}
	},
	required: ['phone'],
}

function setCustomerFields(customerEntity, dto) {
	customerEntity.name = dto.name
	customerEntity.phone = dto.phone
	customerEntity.email = dto.email
}

router.get('/', authorization('CustomersRead'), wrap(async (req, res) => {
	const customers = await Customer.find()
		.select(['-__v'])
		.lean()

	res.json(customers)
}))

router.get('/:id', authorization('CustomersRead'), wrap(async (req, res) => {
	validateObjectID(req.params.id)

	const customer = await Customer.findById(req.params.id)
		.select(['-__v'])
		.lean()

	if (!customer) {
		throw new Error('NotFound')
	}

	res.json(customer)
}))

router.post('/', authorization('CustomersWrite'), wrap(async (req, res) => {
	validateJSON(req.body, customerSchema)

	let customer = await Customer.findOne({ phone: req.body.phone })
	let status = 303
	if (!customer) {
		customer = new Customer()
		setCustomerFields(customer, req.body)
		await customer.save()
		status = 201
	}

	res.set('Location', `${req.baseUrl}/${customer._id}`)
	res.sendStatus(status)
}))

module.exports = router
