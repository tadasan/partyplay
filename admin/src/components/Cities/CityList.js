import React, { useRef, useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import {
	Card,
	ButtonToolbar,
	Table,
	Button,
	ButtonGroup,
} from 'react-bootstrap'
import PlusIcon from 'mdi-react/PlusIcon'
import DeleteIcon from 'mdi-react/DeleteIcon'
import AccountEditIcon from 'mdi-react/AccountEditIcon'
import http from '../../http'
import { useAlerts } from '../../alerts'

function CityList() {
	const history = useHistory()
	const { showAlert } = useAlerts()
	const [cities, setCities] = useState([])
	const [reloadTable, setReloadTable] = useState(false)

	useEffect(() => {
		getCities()
	}, [reloadTable])

	async function getCities() {
		try {
			const response = await http.get('/api/cities')
			setCities(response.data)
		} catch (error) {}
	}

	async function deleteCity(id) {
		try {
			await http.delete('/api/cities/' + id)
			showAlert('Miestas sėkmingai ištrintas', 'success')
			await setReloadTable(!reloadTable)
		} catch (error) {
			if (error.response.data.data === 'CategoryReferenceInCities') {
				return showAlert(
					'Ištrinti nepavyko, nes yra produktų, priklausančių šiam miestui',
					'danger',
				)
			}
			showAlert('Nepavyko ištrinti miesto', 'danger')
		}
	}

	return (
		<>
			<Card>
				<Card.Body>
					<Card.Title>
						<h5>Miestai</h5>
						<ButtonToolbar className="products-list__btn-toolbar-top">
							<Link to="/cities/new">
								<Button variant="secondary" size="sm">
									<PlusIcon /> Kurti naują
								</Button>
							</Link>
						</ButtonToolbar>
					</Card.Title>
					<Table striped hover responsive>
						<thead>
							<tr>
								<th>Miestas</th>
								<th>Adresas</th>
								<th>Veiksmai</th>
							</tr>
						</thead>
						<tbody>
							{cities.map((item) => (
								<tr key={item._id}>
									<td>{item.name}</td>
									<td>{item.address}</td>
									<td>
										<ButtonGroup>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													deleteCity(item._id)
												}}
											>
												<DeleteIcon />
											</a>
											<a
												className="link-click"
												size="lg"
												onClick={() => {
													history.push(`/cities/${item._id}`)
												}}
											>
												<AccountEditIcon />
											</a>
										</ButtonGroup>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		</>
	)
}

export default CityList
