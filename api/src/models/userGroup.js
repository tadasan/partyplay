const mongoose = require('mongoose')

const userGroupSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true,
	},
	rights: [String],
})

userGroupSchema.statics.rights = [
	'UsersRead',
	'UsersWrite',
	'UsersDelete',
	'UserGroupsRead',
	'UserGroupsWrite',
	'UserGroupsDelete',
	'CitiesRead',
	'CitiesWrite',
	'CitiesDelete',
	'CategoriesRead',
	'CategoriesWrite',
	'CategoriesDelete',
	'ProductsRead',
	'ProductsWrite',
	'ProductsDelete',
	'FilesWrite',
	'CustomersRead',
	'CustomersWrite',
	'CustomersDelete',
	'RentalsRead',
	'RentalsWrite',
	'RentalsDelete',
]

const UserGroup = mongoose.model('UserGroup', userGroupSchema)

module.exports = UserGroup
