const router = require('express').Router()
const wrap = require('express-async-wrap')
const { validateJSON, validateObjectID, getPaging } = require('../util')
const { ValidationError } = require('../errors')
const City = require('../models/city')
const Category = require('../models/category')
const Product = require('../models/product')

router.get('/cities', wrap(async (req, res) => {
	const cities = await City.find()
		.select(['-__v'])
		.lean()

	res.json(cities)
}))

router.get('/cities/:cityName/categories', wrap(async (req, res) => {
	const city = await City.findOne({ name: req.params.cityName })
	if (!city) {
		throw new Error('NotFound')
	}

	const categories = (await Category.find({}, ['-__v'])
		.populate({
			path: 'products',
			match: { 'city': city._id },
		})
		.lean()).filter(category => category.products.length !== 0)

	res.json(categories)
}))

module.exports = router
